<?php

namespace Sibneuro\SiteBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sibneuro\SiteBundle\Entity\User;
use Sibneuro\SiteBundle\Entity\Page;
use Sibneuro\SiteBundle\Entity\Order;
use Sibneuro\SiteBundle\Entity\OrderProduct;
use Sibneuro\SiteBundle\Entity\Discount;
use Doctrine\ORM\NoResultException;
use Sibneuro\SiteBundle\Controller\CRUDUser;
use Sibneuro\SiteBundle\Controller\CRUDPage;
use Sibneuro\SiteBundle\Component\Login;
use Sibneuro\SiteBundle\Component\Ispasswordssame;
use Sibneuro\SiteBundle\Form\LoginUsersType;
use Sibneuro\SiteBundle\Form\UsersType;
use Sibneuro\SiteBundle\Form\SearchType;
use Sibneuro\SiteBundle\Component\SearchProperties;
use Sibneuro\SiteBundle\Component\Paginator;

class CommonController extends Controller
{   
          /**
          * @Route("/paginator", name="paginator_route")
          */
      public function paginationAction(Paginator $paginator){
          $pagination = $paginator->pagination();

          if($paginator->route == "admin_search")
            return $this->render('common/paginator.search.html.twig', array(
              'prop' => $paginator->prop,
              'i' => $pagination['start'],
              'j' => $pagination['finish'],
              'prev' => $pagination['prev'],
              'next' => $pagination['next'],
              'num' => $paginator->num,
              'route' => $paginator->route,
              ));
          if($paginator->route == "admin_show")
            return $this->render('common/paginator.show.html.twig', array(
              'i' => $pagination['start'],
              'j' => $pagination['finish'],
              'prev' => $pagination['prev'],
              'next' => $pagination['next'],
              'num' => $paginator->num,
              'route' => $paginator->route,
              'entity' => $paginator->entity
              ));

    

        }

          /**
          * @Route("/", name="common_showmainpage")
          */
      public function showmainpageAction(){   
          $em = $this->getDoctrine()->getManager();
          $page_found = $em->createQueryBuilder('p')
                        ->select('p.pgcontent')
                        ->from('SibneuroSiteBundle:Page', 'p')
                        ->where('p.pgname = :page' )
                        ->setParameters(array(
                            'page' => "Main",
                            ))
                        ->getQuery();
                        $page_found = $page_found->getSingleResult();
                        $twig = clone $this->get('twig');
                        $twig->setLoader(new \Twig_Loader_String());
                        $rendered = $twig->render(
                        htmlspecialchars_decode($page_found['pgcontent'], ENT_QUOTES));

                        return $this->render('common/teastore.html.twig', array(
                          'content' => $rendered));


      }












        /**
        * @Route("/tea/{id}", name="common_teashow")
        */
       public function teashowAction(Request $request, $id){
            $em= $this->getDoctrine()->getManager();
                $product = $em->createQueryBuilder('u')
                        ->select('p')
                        ->from('SibneuroSiteBundle:Product', 'p')
                        ->where('p.id = :id' )
                        ->setParameters(array(
                            'id' => $id,
                            ))
                        ->getQuery();
                  $product = $product->getResult();
                  $form = $this->createFormBuilder()
                    ->add('add','submit', array(
                      'attr' => array(
                        'class' => "btn btn-primary btn-md sr-only"),
                      'label' => 'Add to cart'))
                    ->add('count', 'integer', array(
                        'attr' => array(
                          'class' => 'form-control')
                          ))
                    ->getForm();
                    $form->handlerequest($request);
                    $count = $form->get('count')->getData();
                    if($form->get('add')->isClicked()){
                      return $this->forward('SibneuroSiteBundle:common:addToCart', array(
                          'count' => $count,
                          'id' => $product[0]->getId()
               ));
                    }
                 return $this->render('common/common.product.html.twig',array(
                'product' => $product[0],
                'form' => $form->createView()
                )
            );    
            }
// !!!!!!! fixed it later
    public function getRefererParams() {
        $request = $this->getRequest();
        $referer = $request->headers->get('referer');
        $baseUrl = $this->get('request')->getSchemeAndHttpHost();
        $lastPath = substr($referer, strpos($referer, $baseUrl) + strlen($baseUrl));
        return $this->get('router')->getMatcher()->match($lastPath);
    }
              /**
              * @Route("/add_to_cart/{id}", name="add_to_cart", defaults={
              * "count" = 1
              *})
              *
              */
      public function addToCartAction(Request $request, $count, $id){
               $session = $request->getSession();
                      if($session->get("$id"))
                      $session->set("$id", $session->get("$id")+$count);
                      else
                      $session->set("$id", $count);
                      $params = $this->getRefererParams();
                      if(isset($params['id']))
                      return $this->redirect($this->generateUrl(
                          $params['_route'], array(
                          'id' => $params['id']
                          )
                        ));
                      else
                      return $this->redirect($this->generateUrl(
                          $params['_route']
                        ));
                      }
               
      /**
      * @Route("/cart", name="cart")
      */
      public function cartAction(Request $request){
          if($request->request->get('form')['step2']== true)
            $step2 = true;
          else 
            $step2 = false;
        $em = $this->getDoctrine()->getManager();
        $order = new Order();
        $count = array();
        $total = 0;
        $discount = 0;
        $disc_min = array();
        $session = $request->getSession();
        $all = $session->all();
        $i = 0;
        $products = array();
        foreach($all as $product => $value){
          if(is_int($product)){
            $id = $product;
            $product = $em->createQueryBuilder('p')
            ->select('p')
            ->from('SibneuroSiteBundle:Product', 'p')
            ->where('p.id = :id')
            ->setParameters(array(
                            'id' => $id,
                            ))
            ->getQuery();
            $product = $product->getSingleResult();
            $count[$id] = $value;
              if (!$product) {
throw $this->createNotFoundException('The product does not exist!');
}             
            $products[$i] = $product;
            $i++;
            $total += $product->getPrice() * $value;
            
          }
}
            if(count($products) == 0)
            return $this->render('common/cart.html.twig', array(
              'is_product' => false));
            $order->setTotal($total);

             $discount_min = $em->createQueryBuilder('d')
            ->select('d')
            ->from('SibneuroSiteBundle:Discount', 'd')
            ->where('d.bound < :total') // fix it, bound = 0
            ->setParameters(array(
              'total' => $total
              ))
            ->getQuery();
            $discount_min = $discount_min->getResult();
            if(!isset($discount_min))
              $discount_min = 0;
            else{
              $i = 0;
              foreach($discount_min as $var){
                $disc_min[$i] = $var->getBound();
                $i++;
              }
              if(count($disc_min) != 0){
            $disc_min = max($disc_min);
            foreach($discount_min as $var)
                if($var->getBound() === $disc_min)
                  $order->setDiscount($var);
           }
         }
           $form0 = $this->createFormBuilder()
              ->add('next','submit')
              ->add('step2', 'hidden', array(
                'attr' => array(
                  'value' => false)))
              ->getForm();
            $form0->handlerequest($request);
            if($form0->isValid()||$step2 == true){
           // we have order with array of product
           // total price
           // discount
           // now we need to create form with order description
           // and clients data for new user entity
           $form1 = $this->createFormBuilder()
            ->add('username','text', array(
              'label' => "Выше имя"))
            ->add('email','email', array(
              'label' => "Адрес электронной почты"))
            ->add('phone','number', array(
              'label' => "Телефон"))
            ->add('description','textarea', array(
              'required' => false,
              'label' => "Дополнительная информация о заказе"))
            ->add('submit','submit', array(
              'label' => "Отправить заказ"))
            ->add('step2','hidden', array(
                'attr' => array(
                  'value' => 'true'
              )))
            ->getForm();
            $form1->handlerequest($request);
            if($form1->get('submit')->isClicked()&&$form1->isValid()){
              $order->setDescription($form1->get('description')->getData());
              $customer = new User();
              $customer->setUsername($form1->get('username')->getData());
              $customer->setEmail($form1->get('email')->getData());
              $customer->setPhone($form1->get('phone')->getData());
              $order->setCustomer($customer);
              $customer->setOrder($order);
              // now order and customer are prepared for saving to db
                $em->persist($order);
                $em->persist($customer);
                $em->flush();
              for($i=0;$i<count($products);$i++){
                $orderProduct = new OrderProduct();
                $orderProduct->setProduct($products[$i]);
                $orderProduct->setOrder($order);
              for($v=0;$v<$count[$products[$i]->getId()];$v++){
                $orderProduct->setAmount($orderProduct->getAmount() + 1);
          }
                $em->persist($orderProduct);
                $em->flush($orderProduct);
        }
                $session->clear();
              return $this->render('common/ordershipped.html.twig');
            }

      return $this->render('common/cart.html.twig', array(
          'is_product' => true,
          'products' => $products,
          'total' => $order->getTotal(),
          'discount' => $order->getDiscount(),
          'form1' => $form1->createView()
        ));
}
    return $this->render('common/cart.list.twig', array(
      'form0' => $form0->createView(),
      'products' => $products,
      'discount' => $discount,
      'count' => $count,
      'total' => $total
      ));

      }




              //All hardcoded pages have to be until this function
         
              /**
              * @Route("/{page}", name="common_showotherpage")
              *
              */
      public function showotherpageAction($page){
        if($page === '3dparrot'){
          return $this->render('common/parrot.html.twig');
        }
          $em = $this->getDoctrine()->getManager();
          $page_found = $em->createQueryBuilder('p')
                        ->select('p.pgcontent')
                        ->from('SibneuroSiteBundle:Page', 'p')
                        ->where('p.pgname = :page' )
                        ->setParameters(array(
                            'page' => $page,
                            ))
                        ->getQuery();
                        $page_result= $page_found->getResult();
                        if (!$page_result) {
throw $this->createNotFoundException('The page does not exist');
}
                        else
                         $page_found = $page_found->getSingleResult();
                       // дополнительный загрузчик для рендеринга контента, который
                                    // находится в базе данных
                        $twig = clone $this->get('twig');
                        $twig->setLoader(new \Twig_Loader_String());
                        $rendered = $twig->render(
                        htmlspecialchars_decode($page_found['pgcontent'], ENT_QUOTES));

                        return $this->render('common/teastore.html.twig', array(
                          'content' => $rendered));


      }
            /**
          * @Route("/top_products_ev", name="topproducts")
          */
      public function topproductsAction(){
        
          $em = $this->getDoctrine()->getManager();
          $products_found = $em->createQueryBuilder('pr')
                        ->select('pr')
                        ->from('SibneuroSiteBundle:Product', 'pr')
                        ->setFirstResult(0)->setMaxResults(9)
                        ->getQuery();
                        $products_found = $products_found->getResult();
                        
                        return $this->render('common/topprod.html.twig', array(
                          'products' => $products_found));


      }
         /**
          * @Route("/cart_count", name="cart_count")
          */
      public function cartCountAction(Request $request){
        $session = $request->getSession();
        $cart_count = 0;
        $arr = $session->all();
        foreach($arr as $key => $value)
          if(is_int($value))
            $cart_count++;
        if($cart_count === 0)
          return new Response('');
        return new Response($cart_count);
      }



      public function catalogAction(){
        $em = $this->getDoctrine()->getManager();
        $products_found = $em->createQueryBuilder('pr')
          ->select('pr')
          ->from('SibneuroSiteBundle:Product', 'pr')
          ->getQuery();
          $products_found = $products_found->getResult();

          return $this->render('common/catalog.html.twig', array(
            'products' => $products_found));
      }
     
    }