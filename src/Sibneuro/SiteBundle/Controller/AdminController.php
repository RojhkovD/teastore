<?php

namespace Sibneuro\SiteBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sibneuro\SiteBundle\Entity\User;
use Sibneuro\SiteBundle\Entity\Page;
use Sibneuro\SiteBundle\Entity\File;
use Sibneuro\SiteBundle\Entity\Product;
use Sibneuro\SiteBundle\Entity\Order;
use Sibneuro\SiteBundle\Entity\OrderProduct;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Session\Session;
use Sibneuro\SiteBundle\Controller\CRUDUser;
use Sibneuro\SiteBundle\Controller\CRUDPage;
use Symfony\Component\HttpFoundation\Cookie;
use Sibneuro\SiteBundle\Component\Login;
use Sibneuro\SiteBundle\Component\Ispasswordssame;
use Sibneuro\SiteBundle\Form\LoginUsersType;
use Sibneuro\SiteBundle\Form\UsersType;
use Sibneuro\SiteBundle\Form\PagesType;
use Sibneuro\SiteBundle\Form\FilesType;
use Sibneuro\SiteBundle\Form\OrdersType;
use Sibneuro\SiteBundle\Form\ProductsType;
use Sibneuro\SiteBundle\Form\SearchType;
use Sibneuro\SiteBundle\Component\SearchProperties;
use Sibneuro\SiteBundle\Component\Paginator;
use Sibneuro\SiteBundle\Component\RegexpFunction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
class AdminController extends Controller
{   


    /**
     * @Route("/login", name="login_form")
     * @Template()
     */
    public function loginAction(Request $request){

        $authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('Pages\login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
            'login' => "<script> login(); </script>",
            ));

    }
    // send controll from login.html form
    /**
     * @Route("/login_check")
     */
    public function checkAction(Request $request){

        

        $cookie = $request->cookies;
        $cookie1 = $request->cookies;
        $cookie2 = $request->cookies;
        $request = $this->get('request');

        $session=$this->getRequest()->getSession();
        $em = $this->getDoctrine()->getEntityManager();
        $repository = $em->getRepository('SibneuroSiteBundle:User');
        if($request->getMethod() == 'POST')
        {
        $session=$this->getRequest()->getSession();
        
        $session->clear();
                $email = $request->get('email');
                $password = $request->get('password');
                $remember = $request->get('remember');




        // обращение к базе
        $repository = $this->getDoctrine()->getManager();
        
        // setParameter защита от sql инъекций
        $query = $repository->createQuery(
            'SELECT * 
             FROM SibneuroSiteBundle:User u
             WHERE u.username = :username_email_input
             OR u.email = :username_email_input
             AND u.password = :password_input'
            )->setParameter(array(
                'username_email_input'=>"$email",
                'password_input'=>"$password",
                )); 
        

        $user = $query->getResult();



//надо будет поправить
        if($user){
                $login = new Login();
                $login->setEmail($email);
                $login->setPassword($password);
                $session->set('login', $login);
                
                if($remember == 'y'){
                $response = new Response();
                $cookie1 = new Cookie('login[password]',$password,time() + 3600 * 24 * 7);
                $response->headers->setCookie($cookie1);
                $cookie2 = new Cookie('login[email]',$email,time() + 3600 * 24 * 7);
                $response->headers->setCookie($cookie2);
                
                $response->send();
                }
                return $this->render('admindashboard.html.twig');
           }
            else{
                return new Response('Access denied. Check your login data');
                }
    }else{
    if($cookie1->has('login')&&$cookie2->has('login')){
               
               $login = $cookie1->get('login');
               $login = $cookie2->get('login');
            
               $query = $repository->createQuery(
            'SELECT * 
             FROM SibneuroSiteBundle:User u
             WHERE u.username = :username_email_input
             OR u.email = :username_email_input
             AND u.password = :password_input'
            )->setParameter(array(
                'username_email_input'=>"$login[email]",
                'password_input'=>"$login[password]",
                ));
           $user = $query->getResult();
    
    if($user){
                 return $this->render('admindashboard.html.twig');
    }
    else{
                  
                  return new Response('Access denied. Check your login data');
    }
    }
    }

}
    
       /* if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            throw $this->createAccessDeniedException('Unable to access this page!');
        }*/

        /**
        * @Route("/admin/{entity}/add", name="admin_add")
        */
    public function addAction(Request $request, $entity){
        $em = $this->getDoctrine()->getManager();
        if($entity === "users"){
        $user = new User();
        $form = $this->createForm(new UsersType(), $user);
        if($request->request){
        $form->handleRequest($request);
     }
        if($form->isValid()){
        $password = $form->get('password')->getData();
        $encoder =  $this->container->get('security.password_encoder');
        $encoded =  $encoder->encodePassword($user, $password);
                $user->setPassword($encoded);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

            
            return $this->redirectToRoute('admin_show', array(
                'entity' => "users",
                'num' => '1',
                'request_status' => "Successfully created"));
    }
              return $this->render('admin/adduser.html.twig', array(
                    'form' => $form->createView()));

}
        if($entity === "pages"){
        $page = new Page();
        $form = $this->createForm(new PagesType(), $page);
        if($request->request){
        $form->handleRequest($request);
     }
        if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($page);
                $em->flush();

            
            return $this->redirectToRoute('admin_show', array(
                'entity' => "pages",
                'request_status' => "Successfully created"));
    }
              return $this->render('admin/addpage.html.twig', array(
                    'form' => $form->createView()));

}
    if($entity === "files"){
        $file = new File();
        $form = $this->createForm(new FilesType(), $file);
        if($request->request){
            $form->handleRequest($request);
        }
        if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($file);
                $em->flush();

            
            return $this->redirectToRoute('admin_show', array(
                'entity' => "files",
                'request_status' => "Successfully created"));
        }
          return $this->render('admin/addfile.html.twig', array(
                'form' => $form->createView()));

    }
    if($entity === "products"){
  
        $product = new Product();
        $form = $this->createForm(new ProductsType(), $product);
        if($request->request){
            $form->handleRequest($request);
        }

        if($form->isValid()){
       
                $em = $this->getDoctrine()->getManager();
                $em->persist($product);
                $em->flush();
            
            return $this->redirectToRoute('admin_show', array(
                'entity' => "products",
                'num' => '1',
                'request_status' => "Successfully created"));
        }
              return $this->render('admin/addproduct.html.twig', array(
                    'form' => $form->createView()));
    }

    if($entity === "orders"){

        $order = new Order();
        $form = $this->createForm(new OrdersType($em, $order), $order);
        if($request->request){
            $form->handleRequest($request);
        }
        if($form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $order->setDescription("");
            $order->setTotal(0);

            $customer = new User();

            $customer->setUsername($form->get('customer')->get('username')->getData());
            $customer->setEmail($form->get('customer')->get('email')->getData());
            $customer->setPhone($form->get('customer')->get('phone')->getData());
            $customer->setOrder($order);
            $em->persist($customer);

            // $discount = $em->createQueryBuilder('d')
            // ->select('d')
            // ->from('SibneuroSiteBundle:Discount', 'd')
            // ->where('d.id = 2')    // fix it
            // ->getQuery();
            // $discount = $discount->getSingleResult();

            $order->setDiscount();
            $order->setCustomer($customer);
            $em->persist($order);
            /*$products = $em->createQueryBuilder('d')
            ->select('d')
            ->from('SibneuroSiteBundle:Product', 'd')
            ->where('d.id = 2')    // fix it
            ->getQuery();
            $products = $products->getResult();
            for($i=0;$i<count($products);$i++){
                $orderProduct = new OrderProduct();
                $orderProduct->setProduct($products[$i]);
                $orderProduct->setOrder($order);
              for($v=0;$v<$count[$products[$i]->getId()];$v++){
                $orderProduct->setAmount($orderProduct->getAmount() + 1);
          }
                $em->persist($orderProduct);
                $em->flush($orderProduct);*/
            $em->flush();

            
            return $this->redirectToRoute('admin_show', array(
                'entity' => "orders",
                'num' => '1',
                'request_status' => "Successfully created"));
    }
              return $this->render('admin/addorder.html.twig', array(
                    'form' => $form->createView()));

}
}
                    /**
                    * @Route("admin/{entity}/delete", name="admin_delete")
                    * 
                    */
       public function deleteAction(Request $request, $entity){
        $focused = 0;
        $focused_users = $request->query->get('focused_users');
        $focused_pages = $request->query->get('focused_pages');
        $focused_products = $request->query->get('focused_products');
        $focused_files = $request->query->get('focused_files');
        $focused_orders = $request->query->get('focused_orders');
        $focused_comments = $request->query->get('focused_comments');
        $num = $request->query->get('num');
            $form = $this->createFormBuilder()
                ->setMethod('POST')
                ->add('YES','submit', array(
                    'attr' => array(
                        'id' => 'yes',
                        'class' => 'btn btn-danger btn-md'
                        ),
                    'label' =>"YES"))
                ->add('Cancel','submit', array(
                    'attr' => array(
                        'id' => 'deny',
                        'class' => 'btn btn-default btn-md')))
                ->getForm();
           $form->handleRequest($request);
                switch($entity){
                    case "users":
                    $focused = $focused_users;
                    break;
                    case "files":
                    $focused = $focused_files;
                    break;
                    case "products":
                    $focused = $focused_products;
                    break;
                    case "pages":
                    $focused = $focused_pages;
                    break;
                    case "orders":
                    $focused = $focused_orders;
                    break;
                    case "comments":
                    $focused = $focused_comments;
                    break;
                }
                if($form->get('YES')->isClicked()){
        $em = $this->getDoctrine()->getManager();
            if($entity === "users"){
            $focused_users = $request->query->get('focused_users');
            foreach ($focused_users as $user => $value){
            $users[$user] = $em->getRepository('SibneuroSiteBundle:User')->findOneById($value);
        }   
                /* we will update only one form wich we submited */
            foreach($users as $user){
            $em->remove($user);
            $em->flush();
        }
               return $this->redirectToRoute('admin_show', array(
                'entity' => $entity,
                'num' => $num,
                'status' => "Successfully deleted"));
           }
        if($entity === "pages"){
            $focused_pages = $request->query->get('focused_pages');
            foreach ($focused_pages as $page => $value){
            $pages[$page] = $em->getRepository('SibneuroSiteBundle:Page')->findOneById($value);
        }   
                /* we will update only one form wich we submited */
            foreach($pages as $page){
            $em->remove($page);
            $em->flush();
        }
               return $this->redirectToRoute('admin_show', array(
                'entity' => $entity,
                'num' => $num,
                'status' => "Successfully deleted"));
           }
                if($entity === "files"){
            $focused_files = $request->query->get('focused_files');
            foreach ($focused_files as $file => $value){
            $files[$file] = $em->getRepository('SibneuroSiteBundle:File')->findOneById($value);
        }   
                /* we will update only one form wich we submited */
            foreach($files as $file){
            $em->remove($file);
            $em->flush();
        }
               return $this->redirectToRoute('admin_show', array(
                'entity' => $entity,
                'num' => $num,
                'status' => "Successfully deleted"));
           }
           if($entity == "products"){
                $focused_files = $request->query->get('focused_products');
                foreach($focused_products as $product => $value)
                    $products[$product] = $em->getRepository('SibneuroSiteBundle:Product')->findOneById($value);
                foreach($products as $product){
                    $em->remove($product);
                    $em->flush();
                }
                 return $this->redirectToRoute('admin_show', array(
                'entity' => $entity,
                'num' => $num,
                'status' => "Successfully deleted"));
           }
           if($entity == "orders"){
                $focused_orders = $request->query->get('focused_orders');
                foreach($focused_orders as $order => $value)
                    $orders[$order] = $em->getRepository('SibneuroSiteBundle:Order')->findOneById($value);
                foreach($orders as $order){
                    $em->remove($order);
                    $em->flush();
                }
                 return $this->redirectToRoute('admin_show', array(
                'entity' => $entity,
                'num' => $num,
                'status' => "Successfully deleted"));
           }
            if($entity == "comments"){
                $focused_comments = $request->query->get('focused_comments');
                foreach($focused_comments as $comment => $value)
                    $comments[$comment] = $em->getRepository('SibneuroSiteBundle:Comment')->findOneById($value);
                foreach($comments as $comment){
                    $em->remove($comment);
                    $em->flush();
                }
                 return $this->redirectToRoute('admin_show', array(
                'entity' => $entity,
                'num' => $num,
                'status' => "Successfully deleted"));
           }
           }
                if($form->get('Cancel')->isClicked()){
                    return $this->redirectToRoute('admin_show', array(
                       'entity' => $entity,
                        'num' => $num
                     ));
                }
                return $this->render('admin/warning.html.twig', array(
                        'form' => $form->createView(),
                        'status' => "Do you want to delete these objects?(".count($focused).")",
                    ));
            }
                   /**
                    * @Route("admin/{entity}/update")
                    *
                    */
       public function updateAction(Request $request, $entity){  
        $em = $this->getDoctrine()->getManager();
        
        
        if($entity === "users"){
 
            $message = 'Update user';
            $req = $request->request->get('users');
            $req = $req['c'];

            

            $focused_users = $request->query->get('focused_users');
            foreach ($focused_users as $user => $value){
            $users[$user] = $em->getRepository('SibneuroSiteBundle:User')->findOneById($value);
        }   
                /* we will update only one form wich we submited */
            foreach($users as $user => $value){
            if($req == $users[$user]->getId())
            $forms[$user] = $this->createForm(new UsersType(), $value)->handleRequest($request);
            else 
            $forms[$user] = $this->createForm(new UsersType(), $value);
        }
               
                foreach($forms as $form => $value)
        if($forms[$form]->isValid()){
               foreach($users as $user => $value){
                
                    if($users[$user]->getId() == $req){
                        $em->flush($users[$user]);
                        $message = "Successfully updated";
                        break;
                    }
                }
                }
            foreach($users as $user => $value){
            $forms[$user] = $forms[$user]->createView(); 
        }
            return $this->render('admin/updateuser.html.twig', array(
                'forms' => $forms,
                'users' => $users,
                'title' => $message));
        }
        elseif($entity === "pages"){
            $message = 'Update page';
            $req = $request->request->get('pages');
            $req = $req['c'];

            $focused_pages = $request->query->get('focused_pages');
            foreach ($focused_pages as $page => $value){
            $pages[$page] = $em->getRepository('SibneuroSiteBundle:Page')->findOneById($value);
        }   
                /* we will update only one form wich we submited */
            foreach($pages as $page => $value){
            if($req == $pages[$page]->getId())
            $forms[$page] = $this->createForm(new PagesType(), $value)->handleRequest($request);
            else 
            $forms[$page] = $this->createForm(new PagesType(), $value);
            
        }
               
                foreach($forms as $form => $value)
        if($forms[$form]->isValid()){
               foreach($pages as $page => $value){
                
                    if($pages[$page]->getId() == $req){
                        $em->flush($pages[$page]);
                        $message = "Successfully updated";
                        break;
                    }
                }
                }
            foreach($pages as $page => $value){
            $forms[$page] = $forms[$page]->createView(); 
        }
            return $this->render('admin/updatepage.html.twig', array(
                'forms' => $forms,
                'pages' => $pages,
                'title' => $message));
        }

        elseif($entity === "files"){
            $message = 'Update file';
            $req = $request->request->get('files');
            $req = $req['c'];

            $focused_files = $request->query->get('focused_files');
            foreach ($focused_files as $file => $value){
            $files[$file] = $em->getRepository('SibneuroSiteBundle:File')->findOneById($value);
        }   
                /* we will update only one form wich we submited */
            foreach($files as $file => $value){
            if($req == $files[$file]->getId())
            $forms[$file] = $this->createForm(new FilesType(), $value)->handleRequest($request);
            else 
            $forms[$file] = $this->createForm(new FilesType(), $value);
            
        }
               
                foreach($forms as $form => $value)
        if($forms[$form]->isValid()){
               foreach($files as $file => $value){
                
                    if($files[$file]->getId() == $req){
                        $em->flush($files[$file]);
                        $message = "Successfully updated";
                        break;
                    }
                }
                }
            foreach($files as $file => $value){
            $forms[$file] = $forms[$file]->createView(); 
        }
            return $this->render('admin/updatefile.html.twig', array(
                'forms' => $forms,
                'files' => $files,
                'title' => $message));
        }
        elseif($entity === "products"){
            
            $message = 'Update product';
            $req = $request->request->get('products');
            $req = $req['c'];

             

            $focused_products = $request->query->get('focused_products');
            foreach ($focused_products as $product => $value){
            $products[$product] = $em->getRepository('SibneuroSiteBundle:Product')->findOneById($value);
        }   
                /* we will update only one form wich we submited */
            foreach($products as $product => $value){
            if($req == $products[$product]->getId())
            $forms[$product] = $this->createForm(new ProductsType(), $value)->handleRequest($request);
            else 
            $forms[$product] = $this->createForm(new ProductsType(), $value);
        }
               
                foreach($forms as $form => $value)
        if($forms[$form]->isValid()){
               foreach($products as $product => $value){
                    if($products[$product]->getId() == $req){
                        $em->flush($products[$product]);
                        $message = "Successfully updated";
                        break;
                    }
                }
                }
            foreach($products as $product => $value){
            $forms[$product] = $forms[$product]->createView(); 
        }
            return $this->render('admin/updateproduct.html.twig', array(
                'forms' => $forms,
                'products' => $products,
                'title' => $message));
        }
        elseif($entity === "orders"){
 
            $message = 'Update order';
            $req = $request->request->get('orders');
            $req = $req['c'];


            

            $focused_orders = $request->query->get('focused_orders');
            foreach ($focused_orders as $order => $value){
                $orders[$order] = $em->getRepository('SibneuroSiteBundle:Order')->findOneById($value);
                $orderProducts[$order] = $em->getRepository('SibneuroSiteBundle:OrderProduct')->findByOrder($value);
            }   

                /* we will update only one form wich we submited */
            foreach($orders as $order => $value){
                if($req == $orders[$order]->getId()){
                    $id = $orders[$order]->getId();
                    $forms[$order] = $this->createForm(new OrdersType($em, $orders[$order]), $value)->handleRequest($request);
                }
                else {
                    $id = $orders[$order]->getId();
                    $forms[$order] = $this->createForm(new OrdersType($em, $orders[$order]), $value);
                }
            }

            foreach($forms as $form => $value){
                if($forms[$form]->isValid()){

                    //get total price
                    $order_products = $forms[$form]->get('products')->getData();
                    $total = 0;
                    foreach($order_products as $op){
                        $total += $op->getProduct()->getPrice() * $op->getAmount();
                    }

                    foreach($orders as $order => $value){
                        
                        if($orders[$order]->getId() == $req){
                            $orders[$order]->setTotal($total);
                            $em->persist($orders[$order]);
                            $message = "Successfully updated";
                            break;
                        }
                    }
                    $em->flush();
                }
            }

            foreach($orders as $order => $value){
                $forms[$order] = $forms[$order]->createView(); 
            }

            return $this->render('admin/updateorder.html.twig', array(
                'forms' => $forms,
                'orders' => $orders,
                'title' => $message));
        }
        
        
        
}   
        /**
        * @Route("/admin/show/{entity}/{num}", name="admin_show", defaults={"num"=1})
        */
       public function showAction(Request $request, $entity, $num){
            $em= $this->getDoctrine()->getManager();
            $users_found = null;
            $pages_found = null;
            $files_found = null;
            $products_found = null;
            $orders_found = null;
            $paginator = new Paginator();
            $paginator->route = 'admin_show';
            $paginator->setLimits(9, $num);
            $paginator->entity = $entity;
            switch($entity){
                case 'users':
                $no_limit_users = $em->createQueryBuilder('u')
                        ->select('COUNT(u.id)')
                        ->from('SibneuroSiteBundle:User', 'u')
                        ->getQuery();
                        $no_limit_users_sum = $no_limit_users->getSingleResult();
                $users_found = $em->createQueryBuilder('u')
                        ->select('u')
                        ->from('SibneuroSiteBundle:User', 'u')
                        ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                        ->getQuery();
                        $users_found = $users_found->getResult();
                $paginator->setCount($no_limit_users_sum[1]);
                $all = $request->query->all();
                $paginator->setProp($all);
                return $this->render('admin/admin.listview.html.twig',array(
                'entity' => $entity,
                'num' => $num,
                'paginator' => $paginator,
                'users_found' => $users_found,
                )
            );
                break;
                case 'pages':
                $no_limit_pages = $em->createQueryBuilder('p')
                        ->select('COUNT(p.id)')
                        ->from('SibneuroSiteBundle:Page', 'p')
                        ->getQuery();
                        $no_limit_pages_sum = $no_limit_pages->getSingleResult();
                $pages_found = $em->createQueryBuilder('p')
                        ->select('p')
                        ->from('SibneuroSiteBundle:Page', 'p')
                        ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                        ->getQuery();
                        $pages_found = $pages_found->getResult();
                $paginator->setCount($no_limit_pages_sum[1]);
                $all = $request->query->all();
                $paginator->setProp($all);
                return $this->render('admin/admin.listview.html.twig',array(
                'entity' => $entity,
                'num' => $num,
                'paginator' => $paginator,
                'pages_found' => $pages_found,
                )
            );
                break;
                case 'files':
                $no_limit_files = $em->createQueryBuilder('f')
                        ->select('COUNT(f.id)')
                        ->from('SibneuroSiteBundle:File', 'f')
                        ->getQuery();
                        $no_limit_files_sum = $no_limit_files->getSingleResult();
                $files_found = $em->createQueryBuilder('f')
                        ->select('f')
                        ->from('SibneuroSiteBundle:File', 'f')
                        ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                        ->getQuery();
                        $files_found = $files_found->getResult();
                $paginator->setCount($no_limit_files_sum[1]);
                $all = $request->query->all();
                $paginator->setProp($all);
                 return $this->render('admin/admin.listview.html.twig',array(
                'entity' => $entity,
                'num' => $num,
                'paginator' => $paginator,
                'files_found' => $files_found,
                )
            );
                break;
                case "products":
                $no_limit_products = $em->createQueryBuilder('pr')
                        ->select('COUNT(pr.id)')
                        ->from('SibneuroSiteBundle:Product', 'pr')
                        ->getQuery();
                        $no_limit_products_sum = $no_limit_products->getSingleResult();
                $products_found = $em->createQueryBuilder('pr')
                        ->select('pr')
                        ->from('SibneuroSiteBundle:Product', 'pr')
                        ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                        ->getQuery();
                        $products_found = $products_found->getResult();
                $paginator->setCount($no_limit_products_sum[1]);
                $all = $request->query->all();
                $paginator->setProp($all);
                 return $this->render('admin/admin.listview.html.twig',array(
                'entity' => $entity,
                'num' => $num,
                'paginator' => $paginator,
                'products_found' => $products_found,
                )
            );
                break;
                 case "orders":
                $no_limit_orders = $em->createQueryBuilder('o')
                        ->select('COUNT(o.id)')
                        ->from('SibneuroSiteBundle:Order', 'o')
                        ->getQuery();
                        $no_limit_orders_sum = $no_limit_orders->getSingleResult();
                $orders_found = $em->createQueryBuilder('o')
                        ->select('o')
                        ->from('SibneuroSiteBundle:Order', 'o')
                        ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                        ->getQuery();
                        $orders_found = $orders_found->getResult();
                $orderProduct = $em->createQueryBuilder('p')
                        ->select('p')
                        ->from('SibneuroSiteBundle:OrderProduct', 'p')
                        ->getQuery();
                        $orderProduct = $orderProduct->getResult();
                $paginator->setCount($no_limit_orders_sum[1]);
                $all = $request->query->all();
                $paginator->setProp($all);
                 return $this->render('admin/admin.listview.html.twig',array(
                'entity' => $entity,
                'num' => $num,
                'paginator' => $paginator,
                'orders_found' => $orders_found,
                'orderProducts' =>$orderProduct
                )
            );
                break;
            }

       return null;
}   

                  
/*-------------------------------------------------------*/
/* PAGES CRUD */
































/*------------------------------------------------------*/
      
                /**
                * @Route("/admin/search/{num}", name = "admin_search", 
                * defaults={"num" = 1})
                *
                */
        public function searchAction(Request $request, $num){

            $searchclass = new SearchProperties();
            $searchtype = new SearchType();
            $paginator = new Paginator();
            $paginator->route = 'admin_search';
            $paginator->setLimits(3,$num);
            date_default_timezone_set( "UTC" );
            $form_search = $this->createForm($searchtype, $searchclass);
            $originquery = null;
            $search_pages_by_query_keys = null;
            $search_users_by_query_keys = null;
            $search_products_by_query_keys = null;
            $search_orders_by_query_keys = null;
            $search_files_by_query_keys = null;
            $datestring = null;
            $no_limit_pages_sum = null;
            $no_limit_users_sum = null;
            $no_limit_products_sum = null;
            $no_limit_orders_sum = null;
            $no_limit_files_sum = null;
            $search_result_orders = array();
            $query = null;
            /* this next variables will be defined if we pass it
            there by request->query for pagination without form*/
            $category = $request->query->get('category');
            $datestring = $request->query->get('dateString');
            $datefrom = $request->query->get('dateFrom');
            $originquery = $request->query->get('search');
            $query = explode(' ', $originquery);
            $str = "(";
            foreach($query as $q){
                $str .= "$q|" ;
            }
            $str = substr($str, 0, -1);
            $str .= ")";
            $reg = "^.*$str+.*$";
     
            $em = $this->getDoctrine()->getManager();
            $datefrom = new \DateTime('1970-01-01 00:00:00');
            /* this block will rewrite vars if it was passed by form */
            $form_search->handleRequest($request);
            if($form_search->isValid()){
                $category = $searchclass->category;
                $originquery = $searchclass->search;
                $datestring = $searchclass->dateString;
                $datefrom = $searchclass->dateFrom;
                $query = explode(' ', $originquery); 
                $str = "(";
                foreach($query as $q){
                    $str .= "$q|"; 
                }
                $str = substr($str, 0, -1);
                $str .= ")";
                $reg = "^.*$str+.*$";
                $em= $this->getDoctrine()->getManager();
            }
        
            if(isset($datestring)){
                switch($datestring){
                    case 'all':
                    break;
                    case 'weak':
                    $datefrom = new \DateTime('now - 7day');
                    break;
                    case 'month':
                    $datefrom = new \DateTime('now - 30day');
                    break;
                    case 'year':
                    $datefrom = new \DateTime('now - 365day');
                    break;
                }
            }
        
        
            if(isset($category)){
                switch($category){
                    case 'all':
                        $no_limit_pages = $em->createQueryBuilder('p')
                            ->select('COUNT(p.id)')
                            ->from('SibneuroSiteBundle:Page', 'p')
                            ->where("regexp(:reg, p.pgname) != false")
                            ->orWhere("regexp(:reg, p.pgmeta) != false")
                            ->orWhere("regexp(:reg, p.pgcontent) != false")
                            ->andWhere('p.createdAt > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))->getQuery();
                        $search_result_pages = $em->createQueryBuilder()
                            ->select('p')
                            ->from('SibneuroSiteBundle:Page', 'p')
                            ->where("regexp(:reg, p.pgname) != false")
                            ->orWhere("regexp(:reg, p.pgmeta) != false")
                            ->orWhere("regexp(:reg, p.pgcontent) != false")
                            ->andWhere('p.createdAt > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))
                            ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                            ->getQuery();
                            $no_limit_pages_sum = $no_limit_pages->getSingleResult()[1];
                            $search_pages_by_query_keys = $search_result_pages->getResult();
                        $no_limit_users = $em->createQueryBuilder('u')
                            ->select('COUNT(u.id)')
                            ->from('SibneuroSiteBundle:User', 'u')
                            ->where("regexp(:reg, u.username) != false")
                            ->orWhere("regexp(:reg, u.email) != false")
                            ->orWhere("regexp(:reg, u.address) != false")
                            ->orWhere("regexp(:reg, u.phone) != false")
                            ->andWhere('u.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))->getQuery();
                        $search_result_users = $em->createQueryBuilder()
                            ->select('u')
                            ->from('SibneuroSiteBundle:User', 'u')
                            ->where("regexp(:reg, u.username) != false")
                            ->orWhere("regexp(:reg, u.email) != false")
                            ->orWhere("regexp(:reg, u.address) != false")
                            ->orWhere("regexp(:reg, u.phone) != false")
                            ->andWhere('u.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))
                            ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                            ->getQuery();
                            $no_limit_users_sum = $no_limit_users->getSingleResult()[1];
                            $search_users_by_query_keys = $search_result_users->getResult();
                        $no_limit_products = $em->createQueryBuilder('p')
                            ->select('COUNT(p.id)')
                            ->from('SibneuroSiteBundle:Product', 'p')
                            ->where("regexp(:reg, p.productname) != false")
                            ->orWhere("regexp(:reg, p.category) != false")
                            ->orWhere("regexp(:reg, p.description) != false")
                            ->andWhere('p.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))->getQuery();
                        $search_result_products = $em->createQueryBuilder()
                            ->select('p')
                            ->from('SibneuroSiteBundle:Product', 'p')
                            ->where("regexp(:reg, p.productname) != false")
                            ->orWhere("regexp(:reg, p.category) != false")
                            ->orWhere("regexp(:reg, p.description) != false")
                            ->andWhere('p.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))
                            ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                            ->getQuery();
                            $no_limit_products_sum = $no_limit_products->getSingleResult()[1];
                            $search_products_by_query_keys = $search_result_products->getResult();

                        $no_limit_files = $em->createQueryBuilder('f')
                            ->select('COUNT(f.id)')
                            ->from('SibneuroSiteBundle:File', 'f')
                            ->where("regexp(:reg, f.filename) != false")
                            ->andWhere('f.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))->getQuery();
                        $search_result_files = $em->createQueryBuilder()
                            ->select('f')
                            ->from('SibneuroSiteBundle:File', 'f')
                            ->where("regexp(:reg, f.filename) != false")
                            ->andWhere('f.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))
                            ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                            ->getQuery();
                            $no_limit_files_sum = $no_limit_files->getSingleResult()[1];
                            $search_files_by_query_keys = $search_result_files->getResult();

                        foreach($search_users_by_query_keys as $key => $obj){
                                $no_limit_orders = $em->createQueryBuilder('o')
                                    ->select('COUNT(o.id)')
                                    ->from('SibneuroSiteBundle:Order', 'o')
                                    ->where("o.customer = :user")
                                    ->andWhere('o.created > :datefrom')
                                    ->setParameters(array(
                                        'datefrom' => $datefrom,
                                        'user' => $obj
                                    ))->getQuery();
                                $search_result_orders = array_merge($search_result_orders, $em->createQueryBuilder()
                                    ->select('o')
                                    ->from('SibneuroSiteBundle:Order', 'o')
                                    ->where("o.customer = :user")
                                    ->andWhere('o.created > :datefrom')
                                    ->setParameters(array(
                                        'datefrom' => $datefrom,
                                        'user' => $obj
                                    ))
                                    ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                                    ->getQuery()->getResult());

                                    $no_limit_orders_sum += $no_limit_orders->getSingleResult()[1];
                            }
                            if(is_array($search_result_orders)){
                                $search_orders_by_query_keys = $search_result_orders;
                            }

                    break;

                    case 'users':
                        $no_limit_users = $em->createQueryBuilder('u')
                            ->select('COUNT(u.id)')
                            ->from('SibneuroSiteBundle:User', 'u')
                            ->where("regexp(:reg, u.username) != false")
                            ->orWhere("regexp(:reg, u.email) != false")
                            ->orWhere("regexp(:reg, u.address) != false")
                            ->orWhere("regexp(:reg, u.phone) != false")
                            ->andWhere('u.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))->getQuery();
                        $search_result_users = $em->createQueryBuilder()
                            ->select('u')
                            ->from('SibneuroSiteBundle:User', 'u')
                            ->where("regexp(:reg, u.username) != false")
                            ->orWhere("regexp(:reg, u.email) != false")
                            ->orWhere("regexp(:reg, u.address) != false")
                            ->orWhere("regexp(:reg, u.phone) != false")
                            ->andWhere('u.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))
                            ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                            ->getQuery();
                            $no_limit_users_sum = $no_limit_users->getSingleResult()[1];

                            $search_users_by_query_keys = $search_result_users->getResult();
                    break;

                    case 'pages':
                        $no_limit_pages = $em->createQueryBuilder('p')
                            ->select('COUNT(p.id)')
                            ->from('SibneuroSiteBundle:Page', 'p')
                            ->where("regexp(:reg, p.pgname) != false")
                            ->orWhere("regexp(:reg, p.pgmeta) != false")
                            ->orWhere("regexp(:reg, p.pgcontent) != false")
                            ->andWhere('p.createdAt > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))->getQuery();
                        $search_result_pages = $em->createQueryBuilder()
                            ->select('p')
                            ->from('SibneuroSiteBundle:Page', 'p')
                            ->where("regexp(:reg, p.pgname) != false")
                            ->orWhere("regexp(:reg, p.pgmeta) != false")
                            ->orWhere("regexp(:reg, p.pgcontent) != false")
                            ->andWhere('p.createdAt > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))
                            ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                            ->getQuery();
                            $no_limit_pages_sum = $no_limit_pages->getSingleResult()[1];
                            $search_pages_by_query_keys = $search_result_pages->getResult();
                    break;

                    case 'files':
                        $no_limit_files = $em->createQueryBuilder('f')
                            ->select('COUNT(f.id)')
                            ->from('SibneuroSiteBundle:File', 'f')
                            ->where("regexp(:reg, f.filename) != false")
                            ->andWhere('f.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))->getQuery();
                        $search_result_files = $em->createQueryBuilder()
                            ->select('f')
                            ->from('SibneuroSiteBundle:File', 'f')
                            ->where("regexp(:reg, f.filename) != false")
                            ->andWhere('f.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))
                            ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                            ->getQuery();
                            $no_limit_files_sum = $no_limit_files->getSingleResult()[1];
                            $search_files_by_query_keys = $search_result_files->getResult();
                    break;

                    case 'products':
                        $no_limit_products = $em->createQueryBuilder('p')
                            ->select('COUNT(p.id)')
                            ->from('SibneuroSiteBundle:Product', 'p')
                            ->where("regexp(:reg, p.productname) != false")
                            ->orWhere("regexp(:reg, p.category) != false")
                            ->orWhere("regexp(:reg, p.description) != false")
                            ->andWhere('p.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))->getQuery();
                        $search_result_products = $em->createQueryBuilder()
                            ->select('p')
                            ->from('SibneuroSiteBundle:Product', 'p')
                            ->where("regexp(:reg, p.productname) != false")
                            ->orWhere("regexp(:reg, p.category) != false")
                            ->orWhere("regexp(:reg, p.description) != false")
                            ->andWhere('p.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                                ))
                            ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                            ->getQuery();
                            $no_limit_products_sum = $no_limit_products->getSingleResult()[1];
                            $search_products_by_query_keys = $search_result_products->getResult();
                    break;

                    case 'orders':

                        $no_limit_users = $em->createQueryBuilder('u')
                            ->select('COUNT(u.id)')
                            ->from('SibneuroSiteBundle:User', 'u')
                            ->where("regexp(:reg, u.username) != false")
                            ->orWhere("regexp(:reg, u.email) != false")
                            ->orWhere("regexp(:reg, u.address) != false")
                            ->orWhere("regexp(:reg, u.phone) != false")
                            ->andWhere('u.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                            ))->getQuery();
                        $search_result_users = $em->createQueryBuilder()
                            ->select('u')
                            ->from('SibneuroSiteBundle:User', 'u')
                            ->where("regexp(:reg, u.username) != false")
                            ->orWhere("regexp(:reg, u.email) != false")
                            ->orWhere("regexp(:reg, u.address) != false")
                            ->orWhere("regexp(:reg, u.phone) != false")
                            ->andWhere('u.created > :datefrom')
                            ->setParameters(array(
                                'datefrom' => $datefrom,
                                'reg' => $reg
                            ))
                            ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                            ->getQuery();
                            $no_limit_users_sum = $no_limit_users->getSingleResult()[1];

                            $search_users_by_query_keys = $search_result_users->getResult();

                            foreach($search_users_by_query_keys as $key => $obj){
                                $no_limit_orders = $em->createQueryBuilder('o')
                                    ->select('COUNT(o.id)')
                                    ->from('SibneuroSiteBundle:Order', 'o')
                                    ->where("o.customer = :user")
                                    ->andWhere('o.created > :datefrom')
                                    ->setParameters(array(
                                        'datefrom' => $datefrom,
                                        'user' => $obj
                                    ))->getQuery();
                                $search_result_orders = array_merge($search_result_orders, $em->createQueryBuilder()
                                    ->select('o')
                                    ->from('SibneuroSiteBundle:Order', 'o')
                                    ->where("o.customer = :user")
                                    ->andWhere('o.created > :datefrom')
                                    ->setParameters(array(
                                        'datefrom' => $datefrom,
                                        'user' => $obj
                                    ))
                                    ->setFirstResult($paginator->min)->setMaxResults($paginator->max)
                                    ->getQuery()->getResult());

                                    $no_limit_orders_sum += $no_limit_orders->getSingleResult()[1];
                            }
                            if(is_array($search_result_orders)){
                                $search_orders_by_query_keys = $search_result_orders;
                            }
                    break;

                }
            }
        
    
    

            $isresult_search = false;
            if(isset($search_pages_by_query_keys)){
                foreach ($search_pages_by_query_keys as $key) {
                    if(!empty($key)){
                        $isresult_search = true;
                        break;
                    }
                }  
            }

            if(isset($search_users_by_query_keys)){
                foreach ($search_users_by_query_keys as $key) {
                    if(!empty($key)){
                        $isresult_search = true;
                        break;
                    }
                }   
            }

            if(isset($search_products_by_query_keys)){
                foreach ($search_products_by_query_keys as $key) {
                    if(!empty($key)){
                        $isresult_search = true;
                        break;
                    }
                }   
            }

            if(isset($search_orders_by_query_keys)){
                foreach ($search_orders_by_query_keys as $key) {
                    if(!empty($key)){
                        $isresult_search = true;
                        break;
                    }
                }   
            }

            if(isset($search_files_by_query_keys)){
                foreach ($search_files_by_query_keys as $key) {
                    if(!empty($key)){
                        $isresult_search = true;
                        break;
                    }
                }   
            }

            
            /* if there is no any results */
            if($isresult_search === false){
                $message = sprintf('Nothing was found - %s', $originquery);
                return $this->render('admin.html.twig', array(
                    'request_status'=> $message
                ));
            }

            /* query for uri in paginator */
            $paginator->setCount($no_limit_users_sum + $no_limit_pages_sum + $no_limit_products_sum + $no_limit_orders_sum);
            $all = $request->query->all();
            $paginator->setProp($all);
            $date = date_format($datefrom, 'Y-m-d H:i:s');
            $message = sprintf('For - %s - in %s, from %s :', $originquery, $category, $date);
             if($category === "all"){
                $res = array( 
                    'pages_found' => $search_pages_by_query_keys,
                    'users_found' => $search_users_by_query_keys,
                    'files_found' => $search_files_by_query_keys,
                    'products_found' => $search_products_by_query_keys,
                    'orders_found' => $search_orders_by_query_keys,
                );
                foreach($res as $key => $value){
                    if(empty($value)){
                        unset($res[$key]);
                    }
                }
                $res['request_status'] = $message;
                $res['paginator'] = $paginator;

                return $this->render('admin/admin.listview.html.twig', $res);
             }

            if($category === "users"){
                return $this->render('admin/admin.listview.html.twig', array( 
                    'request_status' => $message,
                    'users_found' => $search_users_by_query_keys,
                    'paginator' => $paginator
                ));
            }

            if($category === "pages"){
                return $this->render('admin/admin.listview.html.twig', array( 
                    'request_status' => $message,
                    'pages_found' => $search_pages_by_query_keys,
                    'paginator' => $paginator
                ));
            }

            if($category === "files"){
                return $this->render('admin/admin.listview.html.twig', array( 
                    'request_status' => $message,
                    'files_found' => $search_files_by_query_keys,
                    'paginator' => $paginator
                ));
            }


            if($category === "products"){
                return $this->render('admin/admin.listview.html.twig', array( 
                    'request_status' => $message,
                    'products_found' => $search_products_by_query_keys,
                    'paginator' => $paginator
                ));
            }

            if($category === "orders"){
                return $this->render('admin/admin.listview.html.twig', array( 
                    'request_status' => $message,
                    'orders_found' => $search_orders_by_query_keys,
                    'paginator' => $paginator
                ));
            }

        }


       /**
       * @Route("admin/crud/", name="crud_route", defaults={
            *  "incomeentity" = "pages",
            * "num" = 1
       *})
       *
       */
        public function crudformAction($incomeentity, $num, Request $request){
           $entity = null;
            $form_submit = $this->createFormBuilder()
                ->setMethod('POST')
                ->setAction($this->generateUrl('crud_route'))
                ->add('incomeentity', 'hidden', array(
                    'attr' => array(
                            'id' => $incomeentity,
                            'value' => $incomeentity
                        )
                    ))
                ->add('num', 'hidden', array(
                    'attr' => array(
                            'id' => $num,
                            'value' => $num
                        )
                    ))
                ->add('update', 'submit', array('label' => 'Update'))
                ->add('delete', 'submit', array('label' => 'Delete'))
                ->add('add', 'submit', array('label' => 'Add'))
                ->add('add_choice', 'choice', array(
                    'choices' => array(
                        'pages' => 'Pages',
                        'files' => 'Files',
                        'products' => 'Products',
                        'orders' => 'Orders',
                        'customers' => 'Customers',
                        'comments' => 'Comments'),
                        'required' => false,
                        'empty_value' => 'Users',
                        'label' => false,
                        ))
                ->getForm();
            
            $form_submit ->handleRequest($request);
            $all_checked = $request->request->get('check_all');
            
            $add = $form_submit->get('add_choice')->getData();
            if(!isset($add))
                $add = "users";
            $current_entity = $request->request->get('form');
            if(isset($current_entity['incomeentity']))
            $current_entity = $current_entity['incomeentity'];
            $current_num = $request->request->get('form');
            if(isset($current_entity['num']))
            $current_num = $current_num['num'];

            $focused_users = $request->request->get('check_users');
            $focused_pages = $request->request->get('check_pages');
            $focused_files = $request->request->get('check_files');
            $focused_products = $request->request->get('check_products');
            $focused_orders = $request->request->get('check_orders');
            $focused_comments = $request->request->get('check_comments');
            if(isset($focused_users) || isset($focused_pages) || isset($focused_files) || isset($focused_products)
                || isset($focused_orders) || isset($focused_comments))
            $focused_array = array($focused_users, $focused_pages, $focused_files, $focused_products, $focused_orders, $focused_comments);

                if(isset($focused_users))
                    $entity = "users";
                if(isset($focused_pages))
                    if($entity)
                        $entity = "mixed";
                    else $entity = "pages";
                if(isset($focused_files))
                    if($entity)
                        $entity = "mixed";
                    else $entity = "files";
                    if(isset($focused_products))
                        if($entity)
                            $entity = "mixed";
                        else $entity = "products";
                    if(isset($focused_orders))
                        if($entity)
                            $entity = "mixed";
                        else $entity = "orders";
                    if(isset($focused_comments))
                        if($entity)
                            $entity = "mixed";
                        else $entity = "comments";
                    if($form_submit->get('update')->isClicked())
                        if(isset($focused_array)){
                        return $this->redirectToRoute('sibneuro_site_admin_update', array(
                        'focused_users' => $focused_users,
                        'focused_pages' => $focused_pages,
                        'focused_files' => $focused_files,
                        'focused_products' => $focused_products,
                        'focused_orders' => $focused_orders,
                        'focused_comments' => $focused_comments,
                        'entity' => $entity,
                        ));
                    }
                    else
                        return $this->redirectToRoute('admin_show', array(
                            'entity' => $current_entity
                            ));
                    
                if($form_submit->get('delete')->isClicked()){
                    if(isset($focused_array)){
                        return $this->redirectToRoute('admin_delete', array(
                        'focused_users' => $focused_users,
                        'focused_pages' => $focused_pages,
                        'focused_files' => $focused_files,
                        'focused_products' => $focused_products,
                        'focused_orders' => $focused_orders,
                        'focused_comments' => $focused_comments,
                        'num' => $num,
                        'entity' => $entity,
                        ));
                    }
                    else
                        return $this->redirectToRoute('admin_show', array(
                            'entity' => $current_entity,
                            'num' => $current_num
                            ));
         }
                if($form_submit->get('add')->isClicked()){
                    if(isset($add))
                        return $this->redirectToRoute('admin_add', array(
                        'entity' => $add,
                        ));
                    else
                        return $this->redirectToRoute('admin_show', array(
                            'entity' => $current_entity,
                            'num' => $current_num
                            ));

                }


                    return $this->render('admin/crudform.html.twig', array('form' => $form_submit->createView()));
            }
        
        public function searchFormAction(Request $request){
            $search = new SearchProperties;
                
                /* сreate form*/
                $form_search = $this->createForm(new SearchType(), $search);
                    
                $form_search->handleRequest($request);
                
                return $this->render('admin/searchform.html.twig', array(
                    'form_search' => $form_search->createView()));
        }


                /**
                * @Route("/admin/{entity}/ckupdate")
                * 
                *
                */
    public function ckupdateAction(){
    $callback = $_GET['CKEditorFuncNum'];
    $file_name = $_FILES['upload']['name'];
    $file_name_tmp = $_FILES['upload']['tmp_name'];
    $file_new_name = '/web/uploads/page_content';
    $full_path = $file_new_name.$file_name;
    $http_path = '/upload/'.$file_name;
    $error = '';
    if( move_uploaded_file($file_name_tmp, $full_path) ) {
    } else {
     $error = 'Some error occured please try again later';
     $http_path = '';
    }
    echo "<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(".$callback.",  \"".$http_path."\", \"".$error."\" );</script>";
}
        }   