<?php
namespace Sibneuro\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as Collection;
use Symfony\Component\Validator\Constraints as Assert;
/**
* Sibneuro\SiteBundle\Entity\Order
* 
* @ORM\Table(name="orders")
* @ORM\Entity(repositoryClass="Sibneuro\SiteBundle\Entity\OrderRepository")
* @ORM\HasLifecycleCallbacks()
*/
    class Order 
    {
            /**
            * @ORM\Column(type="integer")
            * @ORM\Id
            * @ORM\GeneratedValue(strategy="AUTO")
            */
            private $id;
            /**
            * @ORM\OneToMany(targetEntity="OrderProduct", mappedBy="order")
            */
            private $products;
            
             /**
            * @ORM\OneToOne(targetEntity="User", inversedBy="order")
            * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
            **/
            private $customer;
            /**
            * @ORM\ManyToOne(targetEntity="Discount", inversedBy="order")
            * @ORM\JoinColumn(referencedColumnName="id", nullable = true)
            */
            private $discount;
            /**
            * @var \DateTime
            * @ORM\Column(type="datetime")
            */
            private $created;
            /**
            * @var \DateTime
            * @ORM\Column(type="datetime", nullable = true)
            */
            private $expires;
           /**
           * @ORM\Column(type="string")
           */
            private $description;
           /**
            * @ORM\Column(name="is_active", type="boolean")
            */
            private $isActive;
            /**
            * @ORM\Column(type="float")
            */
            private $total;
            



                    public function __construct()
                    {
                        $now = new \DateTime();
                        //при создании пользовтель считается действующим
                        
                    $this->isActive = true;
                    $this->products = new \Doctrine\Common\Collections\ArrayCollection();
                  
                    
                    }
    /**
    * @ORM\PrePersist
    *
    *
    */
    public function setCreatedValue()
    {
        $this->created = new \DateTime();
    }
    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Order
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Set expires
     *
     * @param \DateTime $expires
     * @return Order
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * Get expires
     *
     * @return \DateTime 
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Order
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Order
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add products
     *
     * @param \Sibneuro\SiteBundle\Entity\OrderProduct $product
     */
    public function addProduct(\Sibneuro\SiteBundle\Entity\OrderProduct $product)
    {
        $product->setOrder($this);

        $this->products->add($product);

    }

    /**
     * Remove products
     *
     * @param \Sibneuro\SiteBundle\Entity\OrderProduct $product
     */
    public function removeOrderProduct(\Sibneuro\SiteBundle\Entity\OrderProduct $product)
    {
        $this->products->removeElement($product);
    }

   
    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }


    /**
     * Set discount
     *
     * @param \Sibneuro\SiteBundle\Entity\Discount $discount
     * @return Order
     */
    public function setDiscount(\Sibneuro\SiteBundle\Entity\Discount $discount = null)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return \Sibneuro\SiteBundle\Entity\Discount 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return Order
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    

    /**
     * Set customer
     *
     * @param \Sibneuro\SiteBundle\Entity\User $customer
     * @return Order
     */
    public function setCustomer(\Sibneuro\SiteBundle\Entity\User $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Sibneuro\SiteBundle\Entity\User 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return Order
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set products
     *
     * @param integer $products
     * @return Order
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \Sibneuro\SiteBundle\Entity\OrderProduct $products
     */
    public function removeProduct(\Sibneuro\SiteBundle\Entity\OrderProduct $products)
    {
        $this->products->removeElement($products);
    }
}
