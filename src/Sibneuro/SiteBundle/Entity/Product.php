<?php
namespace Sibneuro\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
/**
* Sibneuro\SiteBundle\Entity\Product
* 
* @ORM\Table(name="products")
* @ORM\Entity(repositoryClass="Sibneuro\SiteBundle\Entity\ProductRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Product
{
            /**
            * @ORM\Column(type="integer")
            * @ORM\Id
            * @ORM\GeneratedValue(strategy="AUTO")
            */
            private $id;
            /**
            * @ORM\Column(type="string", unique=true)
            */
            private $productname;
            /**
            * @ORM\OneToMany(targetEntity="OrderProduct", mappedBy="product")
            */
            private $orders;
            /**
            * @var \DateTime
            * @ORM\Column(type="datetime")
            */
            private $created;
            /**
            * @ORM\Column(name="path_name", type="text")
            */
            private $path;
            /**
            * @Assert\File(maxSize ="6000000")
            */
            private $file;

            private $temp;
             /**
            * @ORM\Column(name="category", type="text")
            */
            private $category;
             /**
            * @ORM\Column(name="price", type="float")
            */
            private $price;
             /**
            * @ORM\Column(name="instock", type="boolean")
            */
            private $instock;
             /**
            * @ORM\Column(name="description", type="text")
            */
            private $description;
             /**
            * @ORM\Column(name="sold", type="integer")
            */
            private $sold;
             /**
            * @ORM\Column(name="viewed", type="integer")
            */
            private $viewed;
             /**
            * @ORM\Column(name="gold", type="integer")
            */
            private $gold;
            /**
            * @ORM\OneToMany(targetEntity="ProductComment", mappedBy="product")
            * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
            */
            private $comments;

            

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productname
     *
     * @param string $productname
     * @return Product
     */
    public function setProductname($productname)
    {
        $this->productname = $productname;

        return $this;
    }



    /**
     * Set created
     * @ORM\PrePersist
     * @param \DateTime $created
     * @return File
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }
    /**
    * @ORM\PrePersist
    *
    *
    */
    public function setCreatedValue()
    {
        $this->created = new \DateTime();
    }
    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

  
    /**
     * Set path
     *
     * @param string $path
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }



    /* Uploading functions */
    /**
    * @return UploadedFile
    */
    public function getFile(){


        return $this->file;


    }
    /**
    * @param UploadedFile $file
    */
    public function setFile(UploadedFile $file = null){


        $this->file = $file;

        if (isset($this->path)) {
// store the old name to delete after the update
        $this->temp = $this->path;
        $this->path = null;
         }


    }
     public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        $products_dir = __DIR__.'/../../../../web/'.$this->getUploadDir();
        if(!file_exists($products_dir)){
            mkdir(__DIR__.'/../../../../web/'.'uploads/products');
        }

        // the absolute directory path where uploaded
        // documents should be saved
        return $products_dir;
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/products';
    }   
                
            /**
            * @ORM\PrePersist()
            * @ORM\PreUpdate()
            */
            public function preUpload(){
                    if (null !== $this->getFile()) {
                // do whatever you want to generate a unique name
            $productname = sha1(uniqid(mt_rand(), true));
            $mime = $this->getFile()->getClientOriginalName();
            $mime = explode('.', $mime);
            $this->path = $productname.'.'.$mime[count($mime)-1];
                    }
            }
            /**
            * @ORM\PostPersist()
            * @ORM\PostUpdate()
            */
        public function upload(){

                    if (null === $this->getFile()) {
                             return;
             }
                
                    $this->createImagePerformance();
                        
                    // check if we have an old image
                    if (isset($this->temp)) {
                    // delete the old image
                    unlink($this->getUploadRootDir().'/'.$this->temp);
                    // clear the temp image path
    
                    $this->file = null;
                }
        }
        /**
        * @ORM\PostRemove()
        */
        public function removeUpload(){
        $file = $this->getAbsolutePath();
                if ($file) {
                unlink($file);
                }
        }

    

    /**
     * Get productname
     *
     * @return string 
     */
    public function getProductname()
    {
        return $this->productname;
    }

    public function createImagePerformance()
    {


        
        $name = $this->path;
                $mime = explode('.', $name);
                $mime1 = $mime[count($mime)-1];
                if($mime1 === 'jpeg'|| $mime1 === 'jpg' || $mime1 === 'jpe'){
                    $image1 = \imagecreatefromjpeg($this->getFile());
                        list($width, $height) = array(\imagesx($image1), \imagesy($image1));
                        $d = $width - $height;
                        if($d > 0){
                            $image2 = \imagecreatetruecolor($width, $height + $d);
                            $white = \imagecolorallocate($image2, 255, 255, 255);
                            \imagefill($image2, 0, 0, $white);
                            \imagecopy($image2, $image1, 0, abs($d/2), 0, 0, $width, $height);
                            \imagejpeg($image2, $this->getAbsolutePath());
                            \imagedestroy($image2);
                        }
                        elseif($d < 0){
                            $image2 = \imagecreatetruecolor($width + abs($d), $height);
                            $white = \imagecolorallocate($image2, 255, 255, 255);
                            \imagefill($image2, 0, 0, $white);
                            \imagecopy($image2, $image1, abs($d)/2, 0, 0, 0, $width, $height);
                            \imagejpeg($image2, $this->getAbsolutePath());
                            \imagedestroy($image2);
                        }
                        elseif($d === 0)
                            $this->getFile()->move($this->getUploadRootDir(), $this->path);
            }
                elseif($mime1 === 'png'){
                    $image1 = \imagecreatefrompng($this->getFile());
                    list($width, $height) = array(\imagesx($image1), \imagesy($image1));
                        $d = $width - $height;
                        if($d > 0){
                            $image2 = \imagecreatetruecolor($width, $height + $d);
                            $white = \imagecolorallocate($image2, 255, 255, 255);
                            \imagefill($image2, 0, 0, $white);
                            \imagecopy($image2, $image1, 0, abs($d)/2, 0, 0, $width, $height);
                            \imagepng($image2, $this->getAbsolutePath());
                            \imagedestroy($image2);
                        }
                        elseif($d < 0){
                            $image2 = \imagecreatetruecolor($width + abs($d), $height);
                            $white = \imagecolorallocate($image2, 255, 255, 255);
                            \imagefill($image2, 0, 0, $white);
                            \imagecopy($image2, $image1, abs($d)/2, 0, 0, 0, $width, $height);
                            \imagepng($image2, $this->getAbsolutePath());
                            \imagedestroy($image2);
                        }
                        elseif($d === 0)
                            $this->getFile()->move($this->getUploadRootDir(), $this->path);
            }

    }

    /**
     * Set category
     *
     * @param string $category
     * @return Product
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;   
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sold
     *
     * @param integer $sold
     * @return Product
     */
    public function setSold($sold)
    {
        $this->sold = $sold;

        return $this;
    }

    /**
     * Get sold
     *
     * @return integer 
     */
    public function getSold()
    {
        return $this->sold;
    }

    /**
     * Set viewed
     *
     * @param integer $viewed
     * @return Product
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;

        return $this;
    }

    /**
     * Get viewed
     *
     * @return integer 
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Set gold
     *
     * @param integer $gold
     * @return Product
     */
    public function setGold($gold)
    {
        $this->gold = $gold;

        return $this;
    }

    /**
     * Get gold
     *
     * @return integer 
     */
    public function getGold()
    {
        return $this->gold;
    }

  

    /**
     * Set instock
     *
     * @param boolean $instock
     * @return Product
     */
    public function setInstock($instock)
    {
        $this->instock = $instock;

        return $this;
    }

     /**
     * Get instock
     *
     * @return boolean 
     */
    public function getInstock()
    {
        return $this->instock;
    }

    public function __construct()
                    {
                    $this->sold = 0;
                    $this->viewed = 0;
                    $this->gold = 0;
                    }


    /**
     * Add comments
     *
     * @param \Sibneuro\SiteBundle\Entity\ProductComment $comments
     * @return Product
     */
    public function addComment(\Sibneuro\SiteBundle\Entity\ProductComment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \Sibneuro\SiteBundle\Entity\ProductComment $comments
     */
    public function removeComment(\Sibneuro\SiteBundle\Entity\ProductComment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

  

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add orders
     *
     * @param \Sibneuro\SiteBundle\Entity\OrderProduct $orders
     * @return Product
     */
    public function addOrder(\Sibneuro\SiteBundle\Entity\OrderProduct $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \Sibneuro\SiteBundle\Entity\OrderProduct $orders
     */
    public function removeOrderProduct(\Sibneuro\SiteBundle\Entity\OrderProduct $orders)
    {
        return $this->orders->removeElement($orders);
    }

    /**
     * Remove orders
     *
     * @param \Sibneuro\SiteBundle\Entity\OrderProduct $orders
     */
    public function removeOrder(\Sibneuro\SiteBundle\Entity\OrderProduct $orders)
    {
        $this->orders->removeElement($orders);
    }
}
