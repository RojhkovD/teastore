<?php
namespace Sibneuro\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
/**
* Sibneuro\SiteBundle\Entity\User
* 
* @ORM\Table(name="users")
* @ORM\Entity(repositoryClass="Sibneuro\SiteBundle\Entity\UserRepository")
* @ORM\HasLifecycleCallbacks()
*/
class User implements AdvancedUserInterface, \Serializable
{
            /**
            * @ORM\Column(type="integer")
            * @ORM\Id
            * @ORM\GeneratedValue(strategy="AUTO")
            */
            private $id;
            /**
            * @ORM\Column(type="string", length=25, unique=false)
            * @ORM\OneToOne(targetEntity="Page", mappedBy = "createdAt", mappedBy = "updatedAt")
            */
            private $username;
            /**
            * @ORM\Column(type="string", length=64, nullable = true)
            */
            private $password;
            /**
            * @ORM\Column(type="string", length=60)
            */
            private $email;
           /**
           * @ORM\Column(type="string", nullable = true)
           */
            private $fullname;
            /**
            * @ORM\Column(type="text", nullable = true)
            */
            private $address;
            /**
            * @ORM\Column(type="integer")
            */
            private $phone;
            /**
            * @ORM\Column(type="string", nullable = true)
            */
            private $role;

            /**
            * @var \DateTime
            * @ORM\Column(type="datetime")
            */
            private $created;
            /**
            * @ORM\Column(name="is_active", type="boolean")
            */
            private $isActive;
            /**
            * @ORM\Column(name="avatar_path", type="text", nullable = true)
            */
            private $avpath;
            /**
            * @Assert\File(maxSize ="6000000")
            */
            private $file;

            private $temp;

            /**
            * @ORM\OneToOne(targetEntity="Order", mappedBy="customer",  cascade={"persist", "remove"})
            **/
            private $order;

                    public function __construct()
                    {
                        //при создании пользовтель считается действующим
                    $this->isActive = true;
                    $this->order = null;
                    }






            // методы интерфейса UserInterface и AdvancedUserInterface
            public function getUsername()
            {
            return $this->username;
            }
            /**
            * @inheritDoc
            */
            public function getSalt()
            {
            // you *may* need a real salt depending on your encoder
            // see section on salt below
            return null;
            }
            /**
            * @inheritDoc
            */
            public function getPassword()
            {
            return $this->password;
            }
            /**
            * @inheritDoc
            */
            public function getRoles()
            {
            return $this->access_level->toArray();
            }
            /**
            * @inheritDoc
            */
            public function eraseCredentials()
            {
            }

            public function isAccountNonExpired()
                {
                return true;
                }
                public function isAccountNonLocked()
                {
                return true;
                }
                public function isCredentialsNonExpired()
                {
                return true;
                }
                public function isEnabled()
                {
                return $this->isActive;
                }






            //методы интерфейса Serializable
            /**
            * @see \Serializable::serialize()
            */
            public function serialize()
            {
            return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            ));
            }
            /**
            * @see \Serializable::unserialize()
            */
            public function unserialize($serialized)
            {
            list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
            }



            



            // методы класса User
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set created
     * @ORM\PrePersist
     * @param \DateTime $created
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }
    /**
    * @ORM\PrePersist
    *
    *
    */
    public function setCreatedValue()
    {
        $this->created = new \DateTime();
    }
    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     * @return User
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string 
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set avpath
     *
     * @param string $avpath
     * @return User
     */
    public function setAvpath($avpath)
    {
        $this->avpath = $avpath;

        return $this;
    }

    /**
     * Get avpath
     *
     * @return string 
     */
    public function getAvpath()
    {
        return $this->avpath;
    }



    /* Uploading functions */
    /**
    * @return UploadedFile
    */
    public function getFile(){


        return $this->file;


    }
    /**
    * @param UploadedFile $file
    */
    public function setFile(UploadedFile $file = null){


        $this->file = $file;

        if (isset($this->avpath)) {
// store the old name to delete after the update
        $this->temp = $this->avpath;
        $this->avpath = null;
}


    }
     public function getAbsolutePath()
    {
        return null === $this->avpath
            ? null
            : $this->getUploadRootDir().'/'.$this->avpath;
    }

    public function getWebPath()
    {
        return null === $this->avpath
            ? null
            : $this->getUploadDir().'/'.$this->avpath;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/users_av';
    }   
                
            /**
            * @ORM\PrePersist()
            * @ORM\PreUpdate()
            */
            public function preUpload(){
                    if (null !== $this->getFile()) {
                // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $mime = $this->getFile()->getClientOriginalName();
            $mime = explode('.', $mime);
            $this->avpath = $filename.'.'.$mime[count($mime)-1];
                    }
            }
            /**
            * @ORM\PostPersist()
            * @ORM\PostUpdate()
            */
        public function upload(){

                    if (null === $this->getFile()) {
                             return;
             }
                

                $this->getFile()->move($this->getUploadRootDir(), $this->avpath);


                    // check if we have an old image
                    if (isset($this->temp)) {
                    // delete the old image
                    unlink($this->getUploadRootDir().'/'.$this->temp);
                    // clear the temp image path
    
                    $this->file = null;
                }
        }
        /**
        * @ORM\PostRemove()
        */
        public function removeUpload(){
        $file = $this->getAbsolutePath();
                if ($file) {
                unlink($file);
                }
        }

   
   

    

    /**
     * Set phone
     *
     * @param integer $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return integer 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set order
     *
     * @param \Sibneuro\SiteBundle\Entity\Order $order
     * @return User
     */
    public function setOrder(\Sibneuro\SiteBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Sibneuro\SiteBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }
}
