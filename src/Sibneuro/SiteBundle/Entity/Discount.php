<?php
namespace Sibneuro\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as Collection;
use Symfony\Component\Validator\Constraints as Assert;
/**
* Sibneuro\SiteBundle\Entity\Discount
* 
* @ORM\Table(name="discounts")
* @ORM\Entity(repositoryClass="Sibneuro\SiteBundle\Entity\DiscountRepository")
* @ORM\HasLifecycleCallbacks()
*/
class Discount
{
            /**
            * @ORM\Column(type="integer")
            * @ORM\Id
            * @ORM\GeneratedValue(strategy="AUTO")
            */
            private $id;
            /**
            * @ORM\OneToMany(targetEntity="Order", mappedBy="discount")
            **/
            private $orders;
            /**
            * @ORM\Column(type="string")
            */
            private $name;
            /**
            * @ORM\Column(type="integer")
            */
            private $bound;
            /**
            * @ORM\Column(type="float")
            */
            private $value;
           /**
           * @ORM\Column(type="string")
           */
            private $description;
           
            



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Discount
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return Discount
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Discount
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add orders
     *
     * @param \Sibneuro\SiteBundle\Entity\Order $orders
     * @return Discount
     */
    public function addOrder(\Sibneuro\SiteBundle\Entity\Order $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \Sibneuro\SiteBundle\Entity\Order $orders
     */
    public function removeOrder(\Sibneuro\SiteBundle\Entity\Order $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set bound
     *
     * @param integer $bound
     * @return Discount
     */
    public function setBound($bound)
    {
        $this->bound = $bound;

        return $this;
    }

    /**
     * Get bound
     *
     * @return integer 
     */
    public function getBound()
    {
        return $this->bound;
    }
}
