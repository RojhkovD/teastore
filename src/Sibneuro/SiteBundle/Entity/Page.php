<?php 
namespace Sibneuro\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
	/**
	* @ORM\Table(name="pages")
	* @ORM\Entity
	* @ORM\HasLifecycleCallbacks
	*/
	class Page {

		/**
		* @ORM\Id
		* @ORM\Column(type="integer")
		* @ORM\GeneratedValue(strategy="AUTO")
		*/
		private $id;


		/**
		* @ORM\Column(type="string")
		*/
		private $pgname;

		/**
		* @ORM\Column(type="text")
		*/
		private $pgcontent;
        /**
        * @ORM\Column(type="text")
        */
        private $pgmeta;
		/**
		* @ORM\Column(type="string")
		* @ORM\OneToOne(targetEntity="User", inversedBy = "username")
		* @ORM\JoinColumn(name="created_id", referencedColumnName="id")
		*/
		private $createdBy;
		/**
		* @var \DateTime
		* @ORM\Column(type="datetime")
		*/
		private $createdAt;
		
		/**
		* @var \DateTime
		* @ORM\Column(type="datetime")
		*/
		private $updatedAt;

		
	
    /**
     * Get id
     *
     * @return \intager 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pgname
     *
     * @param string $pgname
     * @return Page
     */
    public function setPgname($pgname)
    {
        $this->pgname = $pgname;

        return $this;
    }

    /**
     * Get pgname
     *
     * @return string 
     */
    public function getPgname()
    {
        return $this->pgname;
    }

    /**
     * Set pgcontent
     *
     * @param string $pgcontent
     * @return Page
     */
    public function setPgcontent($pgcontent)
    {
        $this->pgcontent = $pgcontent;

        return $this;
    }

    /**
     * Get pgcontent
     *
     * @return string 
     */
    public function getPgcontent()
    {
        return $this->pgcontent;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return Page
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedBy
     *
     * @param string $updatedBy
     * @return Page
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return string 
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
   
    

    /**
     * Set createdAt
     *
     * 
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();

    }

    /**
     * Set updatedAt
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();

    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Page
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Page
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Set pgmeta
     *
     * @param string $pgmeta
     * @return Page
     */
    public function setPgmeta($pgmeta)
    {
        $this->pgmeta = $pgmeta;

        return $this;
    }

    /**
     * Get pgmeta
     *
     * @return string 
     */
    public function getPgmeta()
    {
        return $this->pgmeta;
    }
}
