<?php
namespace Sibneuro\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as Collection;
use Symfony\Component\Validator\Constraints as Assert;
/**
* Sibneuro\SiteBundle\Entity\ProductComment
* @ORM\Table(name="productComments")
* @ORM\Entity(repositoryClass="Sibneuro\SiteBundle\Entity\ProductCommentRepository")
* @ORM\HasLifecycleCallbacks()
*/
class ProductComment
{
            /**
            * @ORM\Column(type="integer")
            * @ORM\Id
            * @ORM\GeneratedValue(strategy="AUTO")
            */
            private $id;
            /**
            * @ORM\ManyToOne(targetEntity="Product", inversedBy="comments")
            * @ORM\JoinColumn(referencedColumnName="id")
            **/
            private $product;
            /**
            * @ORM\Column(type="string")
            **/
            private $customer;
            /**
            * @ORM\Column(type="string")
            */
            private $name;
            /**
            * @ORM\Column(type="text")
            */
            private $text;
            /**
            * @var \DateTime
            * @ORM\Column(type="datetime")
            */
            private $created;
            /**
            * @var \DateTime
            * @ORM\Column(type="datetime")
            */
            private $updated;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
    }

  
    /**
    * @ORM\PrePersist
    *
    *
    */
    public function setPersistCreatedValue()
    {
        $this->created = new \DateTime();
    }
    /**
    * @ORM\PreUpdate
    *
    *
    */
    public function setUpdateCreatedValue()
    {
        $this->updated = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductComment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return ProductComment
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductComment
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return ProductComment
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set product
     *
     * @param \Sibneuro\SiteBundle\Entity\Product $product
     * @return ProductComment
     */
    public function setProduct(\Sibneuro\SiteBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Sibneuro\SiteBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set customer
     *
     * @param string $customer
     * @return ProductComment
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return string 
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
