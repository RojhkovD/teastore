<?php
namespace Sibneuro\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
/**
* Sibneuro\SiteBundle\Entity\File
* 
* @ORM\Table(name="files")
* @ORM\Entity(repositoryClass="Sibneuro\SiteBundle\Entity\FileRepository")
* @ORM\HasLifecycleCallbacks()
*/
class File
{
            /**
            * @ORM\Column(type="integer")
            * @ORM\Id
            * @ORM\GeneratedValue(strategy="AUTO")
            */
            private $id;
            /**
            * @ORM\Column(type="string", length=25, unique=true)
            * 
            */
            private $filename;
            /**
            * @var \DateTime
            * @ORM\Column(type="datetime")
            */
            private $created;
            /**
            * @ORM\Column(name="path_name", type="text")
            */
            private $path;
            /**
            * @Assert\File(maxSize ="6000000")
            */
            private $file;

            private $temp;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return File
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }



    /**
     * Set created
     * @ORM\PrePersist
     * @param \DateTime $created
     * @return File
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }
    /**
    * @ORM\PrePersist
    *
    *
    */
    public function setCreatedValue()
    {
        $this->created = new \DateTime();
    }
    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

  
    /**
     * Set path
     *
     * @param string $path
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }



    /* Uploading functions */
    /**
    * @return UploadedFile
    */
    public function getFile(){


        return $this->file;


    }
    /**
    * @param UploadedFile $file
    */
    public function setFile(UploadedFile $file = null){


        $this->file = $file;

        if (isset($this->path)) {
// store the old name to delete after the update
        $this->temp = $this->path;
        $this->path = null;
         }


    }
     public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/files';
    }   
                
            /**
            * @ORM\PrePersist()
            * @ORM\PreUpdate()
            */
            public function preUpload(){
                    if (null !== $this->getFile()) {
                // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $mime = $this->getFile()->getClientOriginalName();
            $mime = explode('.', $mime);
            $this->path = $filename.'.'.$mime[count($mime)-1];
                    }
            }
            /**
            * @ORM\PostPersist()
            * @ORM\PostUpdate()
            */
        public function upload(){

                    if (null === $this->getFile()) {
                             return;
             }
                

                $this->getFile()->move($this->getUploadRootDir(), $this->path);


                    // check if we have an old image
                    if (isset($this->temp)) {
                    // delete the old image
                    unlink($this->getUploadRootDir().'/'.$this->temp);
                    // clear the temp image path
    
                    $this->file = null;
                }
        }
        /**
        * @ORM\PostRemove()
        */
        public function removeUpload(){
        $file = $this->getAbsolutePath();
                if ($file) {
                unlink($file);
                }
        }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }
}
