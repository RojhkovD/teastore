//CKEDITOR.config.extraPlugins = 'dialog';
//CKEDITOR.config.extraPlugins = 'widget';
//CKEDITOR.config.extraPlugins = 'image2';
var CKEDITOR_BASEPATH = '../../bundles/sibneurosite/libs/ckeditor/';


function toggleCheckbox(element){
	var x = document.getElementsByClassName('check_all');
    if(element.checked == true)
    	for(var i = 0;i<x.length;i++)
    		x[i].checked = true;
    else
    	for(var i = 0;i<x.length;i++)
    		x[i].checked = false;
}



function bindItemPrice(form_group){

    $($(form_group).find(':input')[0]).children().each(function(i){

        if($(this).parent()[0].value === $(this).attr('value')){
            $(form_group).find(':input')[1].value = parseInt(prices[i]).toFixed(2);
            calcTotal();
        }
    });

    $($(form_group).find(':input')[0]).on('change', function(){
        var itemPrice = 0;
        $(this).children().each(function(i){

            if($(this).parent()[0].value === $(this).attr('value')){
                itemPrice = prices[i];
            }

        });
        $(form_group).find(':input')[1].value = parseInt(itemPrice).toFixed(2);
        calcTotal();
    });

    $($(form_group).find(':input')[2]).on('change', function(){

        if($(this)[0].value%1){
            $(this)[0].value = Math.round($(this)[0].value);
        }
        
        calcTotal();
    });

}    

function calcTotal(){
    var collection = $('ul.products');
    collection.each(function(){
        var total = 0;
        $(this).find('.products_collection').each(function(){
            total += $(this).find(':input')[1].value * Math.abs($(this).find(':input')[2].value);
        });
        $(this).parent().find('.total_order').attr('value', parseInt(total).toFixed(2));
    });
}


//build price array

var prices = (function(){

    var prices = [];
    var priceList = $('#orders_price_list');

    priceList.find(':input').each(function(i){
        prices[i] = $(this).attr('value');
    });

    return prices;

}());

jQuery(document).ready(function() {


    //set price changer on each product form on page
    $('.products_collection').each(function(){
        bindItemPrice($(this));
    });

    //set total initial price of order
    calcTotal();

    var $addProductLink = $('<a href="#" class="add_product_link">Add a product</a>');
    var $newLinkLi = $('<div style="float:left; width:100%;"><li style="list-style-type: none;"></li></div>').append($addProductLink);

    // Get the ul that holds the collection of tags
    var $collectionHolder = $('ul.products');
    $collectionHolder.toArray().forEach(function(a){
        $cloneLink = $newLinkLi.clone();
        $(a).append($cloneLink);
        $(a).data('index', $(a).find(':input').length/3);
        $cloneLink.find('.add_product_link').on('click', (function(link) {
            return function(e){
                // prevent the link from creating a "#" on the URL
                e.preventDefault();
                
                // add a new tag form (see code block below)
                addProductForm($(a), link);
            }

        }($cloneLink)));
        
    });


    $collectionHolder = $('div.products_collection');
    $collectionHolder.append('<a style="position:relative; right:-530px; "href="#" class="remove-product">Delete</a>');
    $('.remove-product').click(function(e) {
        e.preventDefault();
        
        $(this).parent().remove();
        
        calcTotal();

        return false;
    });

    

    $('.menu-search_block').on('click', function(){
        console.log('wefew');
        $('.search_bar').toggle();
    });
});
var temp = 0;

function divStyleChange(str, p1, p2){
    	if(temp == 0){
    		temp++;
    		return 'class="form-group" style="width:65%; float:left; margin-bottom:5px;"';
    	}
    	else if(temp == 1){
    		temp++;
    		return 'class="form-group" style="width:20%; float:left; margin-bottom:5px;"';
    	}else{
            temp = 0;
            return 'class="form-group" style="width:15%; float:left; margin-bottom:5px;"'
        }
    }
function addProductForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var rep = prototype.replace(/class="form-group"/g, divStyleChange);
    

    // Replace '$$name$$' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = $.parseHTML(rep.replace(/__name__/g, index));


    $($(newForm).find(':input')[2]).attr('value', 1);


    bindItemPrice(newForm);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<div style="float:left; width:100%; margin-bottom:5px; margin-left:0;"  class="products_collection"><li style="list-style-type: none;"></li></div>').append(newForm);
    
    // also add a remove button, just for this example
    $newFormLi.append('<a style="position:relative; right:-530px;" href="#" class="remove-product">Delete</a>');
    
    $newLinkLi.before($newFormLi);

    calcTotal();

    // handle the removal, just for this example
    $('.remove-product').click(function(e) {
        e.preventDefault();
        
        $(this).parent().remove();

        calcTotal();
        
        return false;
    });
}
