<?php

namespace Sibneuro\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder->add('c','hidden', array(
	  'attr' => array(
	  'id' => 'c_product',
	  ),
	  'mapped' => false));
	  $builder->add('productname','text',array(
	  'attr' => array(
	  'id' => 'productname_product',
	  'placeholder' => 'Product name',

	  ),
	  'required' => true,
	  'label' => false
	  )
	  );
	   $builder->add('category','text',array(
	  'attr' => array(
	  'id' => 'category_product',
	  'placeholder' => 'Category',

	  ),
	  'required' => true,
	  'label' => 'Category'
	  ));
	   $builder->add('description','textarea',array(
	  'attr' => array(
	  'id' => 'description_product',
	  'placeholder' => 'Description',

	  ),
	  'required' => false,
	  'label' => 'Description'
	  ));
	   $builder->add('price','money',array(
	  'attr' => array(
	  'id' => 'price_product',
	  'placeholder' => 'Price',

	  ),
	  'required' => true,
	  'label' => 'Price'
	  ));
	  $builder->add('instock','checkbox',array(
	  'attr' => array(
	  'id' => 'instock_product',
	  ),
	  'required' => true,
	  'label' => 'In stock'
	  ));
	  $builder->add('file','file', array(
	  'attr' => array(
	  'placeholder' => 'Upload image',
	  'id' => 'file',
	  ),
	  
	  'required' => false,
	  'label' => 'Upload image'
	  ));
	  $builder->add('viewed','integer',array(
	  'attr' => array(
	  'id' => 'viewed_product',
	  'placeholder' => 'Viewed',

	  ),
	  'required' => true,
	  'label' => 'Viewed'
	  ));
	  $builder->add('sold','integer',array(
	  'attr' => array(
	  'id' => 'sold_product',
	  'placeholder' => 'Sold',

	  ),
	  'required' => true,
	  'label' => 'Sold'
	  ));
	  $builder->add('submit','submit', array(
	  'attr' => array(
	  'class' => 'btn btn-primary',
	  ),
	  'label' => 'Upload'
	  ));
        
     
    }

    public function getName()
    {
        return 'products';
    }
     public function getDefaultOptions(array $options)
{
    return array(
        'data_class' => 'Sibneuro\SiteBundle\Entity\Product',
    );
}
}
