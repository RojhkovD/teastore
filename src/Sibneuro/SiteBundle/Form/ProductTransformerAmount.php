<?php
namespace Sibneuro\SiteBundle\Form;

use Sibneuro\SiteBundle\Entity\OrderProduct;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ProductTransformerAmount implements DataTransformerInterface
{
    private $entityManager;

    private $id;

    private $products;

    private $amount;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * 
     *
     * @param  Product|null $product
     * @return int
     */
    public function transform($product)
    {
        var_dump($product);
        $this->id = $product->getOwner()->getId();
        foreach($product as $key => $value){
        $this->amount[$key] = $value['value']->getAmount();
        $this->products[$key] = $value['value']->getProduct();
    }
        if (null === $product[0]) {
            return '';
        }
        for($i=0;$i<count($product);$i++){
            $record = $this->entityManager
            ->getRepository('SibneuroSiteBundle:OrderProduct')
            ->findOneBy(
                array('order' => $this->id, 'product' => $this->products[$i]->getId()));
            $arr[$i] = $record->getAmount();

        }
        return $arr;
    }

    /**
     * 
     *
     * @param  string $product
     * @return ProductOrder|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($product)
    {
        // no issue number? It's optional, so that's ok
        if (!$product[0]) {
            return;
        }
        $products = $this->entityManager
            ->getRepository('SibneuroSiteBundle:Product')
            // query for the issue with this id
            ->findByProductname($product);

            foreach($this->products as $key => $value)
        $id_productsOld[$key] = $value->getId();
            foreach($products as $key => $value)
        $id_products[$key] = $value->getId();
        $orderproductOld = $this->EntityManager
            ->getRepository('SibneuroSiteBundle:OrderProduct')
            ->findBy(
                array('order' => $this->id, 'product' => $id_productsOld, 'amount' => $this->amount));

        $orderproduct = $this->entityManager->getRepository('SibneuroSiteBundle:OrderProduct')
            ->findBy(
                array('order' => $this->id, 'product' => $id_products, 'amount' => $this->amount));

        if (null === $orderproduct) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A product  "%s" does not exist!',
                $orderproduct
            ));
        }

        return $orderproduct;
    }
}

?>
