<?php
// src/Blogger/BlogBundle/Form/EnquiryType.php

namespace Blogger\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EnquiryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id','integer');
        $builder->add('firstname','text');
        $builder->add('lastname','text');
        $builder->add('email', 'text');
        $builder->add('password','text');
        $builder->add('gender','text');
        $builder->add('birthday','date');
    }

    public function getName()
    {
        return 'id';
    }
}