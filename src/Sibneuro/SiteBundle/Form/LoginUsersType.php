<?php

namespace Project\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginUsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'email', array(
           'attr' => array(
           'id'  => 'mail',
           'placeholder'  => 'Email',
            ),
           'required' => 'true',
            )
    );
        $builder->add('password', 'password', array(
           'attr' => array(
           'id'  => 'password',
           'placeholder'  => 'Password',
            ),
           'required' => 'true',
            )
    );
    }

    public function getName()
    {
        return 'loginusers';
    }
     public function getDefaultOptions(array $options)
{
    return array(
        'data_class' => 'Project\UserBundle\Entity\Users',
    );
}
}
