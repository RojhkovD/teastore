<?php

namespace Sibneuro\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class OrderUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

      $builder->add('username', 'text', array(
        'attr' => array(
            'id' => 'customer_id'
        )
      ));
      $builder->add('phone', 'number', array(
        'attr' => array(
          'attr' => 'customer_phone'
        )
      ));
      $builder->add('email', 'email', array(
        'attr' => array(
          'attr' => 'customer_email'
        )
      ));
	}
    public function getName()
    {
        return 'orderUser';
    }
}
