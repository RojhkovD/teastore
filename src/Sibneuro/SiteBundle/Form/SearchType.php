<?php
namespace Sibneuro\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {   
        $builder->setMethod('GET');
        $builder->add('search', 'search', array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'Search')))
                    ->add('category', 'choice', array(
                        'choices' => array(
                            'all' => 'All categories',
                            'users' => 'Users',
                            'pages' => 'Pages',
                            'files' => 'Files',
                            'products' => 'Products',
                            'orders' => 'Orders',
                            ),
                        'empty_value' => 'Category',
                        'required'    => false,
                        'empty_data'  => null,
                        'label' => false,
                        'attr' => array(
                                'id' => 'searchcategory',
                            )
                        ));
                    $builder->add('dateString', 'choice', array(
                                'choices' => array(
                                    'all' => 'For all time',
                                    'weak' => 'Last weak',
                                    'month' => 'Last month',
                                    'year' => 'Last year',
                                    
                                ),
                                'required'    => false,
                                'empty_value' => 'Search from',
                                'empty_data'  => null,
                                'label' => false,
                                'attr' => array(
                                'id' => 'searchdate',
                            )));
                    $builder->add('submit', 'submit', array(
                        'attr' => array(
                            'class' => 'col-xs-12 btn btn-primary glyphicon glyphicon-search',
                            ),
                        'label' => ' ', ));
    

     }

    public function getName()
    {
        return 'SearchProperties';
    }
     public function getDefaultOptions(array $options)
{
    return array(
        'data_class' => 'Sibneuro\SiteBundle\Entity\SearchProperies',
        'action' => '/admin/search',
        'method' => 'GET',
    );
}
}

?>




