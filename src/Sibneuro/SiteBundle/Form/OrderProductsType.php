<?php

namespace Sibneuro\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\OptionsResolver\OptionsResolver;


class OrderProductsType extends AbstractType
{
	private $em;

	public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $arr_names = array();
    	$arr = $this->em->getRepository('SibneuroSiteBundle:Product')->findAll();
    	foreach($arr as $product => $value){
            $arr_names[$value->getProductname()] = $value->getProductname();
    		unset($arr[$product]);
    	}

        $builder->add('value', 'choice', array(
        	'label' => false,
        	'choices' => $arr_names,
        ));
        $builder->add('price', 'money', array(
            'label' => false,
            'required' => false,
            'disabled' => true
        ));
        $builder->add('amount', 'integer', array(
        	'label' => false,
        ));
	}
    public function getName()
    {
        return 'order_products';
    }
     public function setDefaultOprions(OptionsResolverInterface $resolver)
{
    $resolver->setDefaults(
    	array(
        'data_class' => 'Sibneuro\SiteBundle\Entity\OrderProduct',
    ));
}
}
