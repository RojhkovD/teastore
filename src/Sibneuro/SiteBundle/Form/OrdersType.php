<?php

namespace Sibneuro\SiteBundle\Form;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class OrdersType extends AbstractType
{
	private $entityManager;

	private $order;

	public function __construct(EntityManager $entityManager, $order)
    {
        $this->entityManager = $entityManager;
        $this->order = $order;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {	
		$builder->add('products', 'collection', array( 
			'type' => new OrderProductsType($this->entityManager),
			'allow_add'    => true,  // make form the form collection know that it will receive an unknown number of tags. So far you've added two tags and the form type expects to receive exactly two, otherwise an error will be thrown: This form should not contain extra fields. To make this flexible, add the allow_add option to your collection field
			//In addition to telling the field to accept any number of submitted objects, the allow_add also makes a "prototype" variable available to you. This "prototype" is a little "template" that contains all the HTML to be able to render any new "tag" forms. To render it, make the following change to your template:
			'allow_delete'    => true,
			'by_reference' => false,
			)
		);

	    $arr_prices = array();
		$arr = $this->entityManager->getRepository('SibneuroSiteBundle:Product')->findAll();
		foreach($arr as $product => $value){
	        $arr_prices[$value->getId()] = $value->getPrice();
			unset($arr[$product]);
		}
		$builder->add('price_list','collection', array(
		'type' => 'hidden',
		'data' => $arr_prices,
		'attr' => array(
		'id' => 'price_list_orders',
		),
		'mapped' => false));
		$builder->add('c','hidden', array(
		'attr' => array(
		'id' => 'c_orders',
		),
		'mapped' => false));
		$builder->add('id','hidden', array(
		'attr' => array(
		'id' => 'id_orders',
		),
		'mapped' => false));
		$builder->add('customer',new OrderUserType(),array(
			'data_class' => 'Sibneuro\SiteBundle\Entity\User',
		'attr' => array(
		'id' => 'customer_order',
		'placeholder' => 'Customer',

		),
		'required' => true,
		'label' => 'Customer'
		));
		$builder->add('discount',new OrderDiscountType(),array(
			'data_class' => 'Sibneuro\SiteBundle\Entity\Discount',
		'attr' => array(
		'id' => 'discount_order',
		'placeholder' => 'Discount',

		),
		'required' => false,
		'label' => 'Discaunt'
		));
		$builder->add('created','datetime',array(
		'attr' => array(
		'id' => 'created_order',
		'placeholder' => 'Created',

		),
		'required' => true,
		'label' => 'Created'
		));
		$builder->add('isActive','checkbox',array(
		'attr' => array(
		'id' => 'isactive_order',
		),
		'required' => true,
		'label' => 'Is active'
		));
		$builder->add('description','textarea', array(
		'attr' => array(
		'placeholder' => 'Description',
		'id' => 'description_order',
		),

		'required' => false,
		'label' => 'Description'
		));
		$builder->add('expires','datetime',array(
		'attr' => array(
		'id' => 'expires_order',
		'placeholder' => 'Expires',

		),
		'required' => false,
		'label' => 'Expires'
		));
		$builder->add('total','money',array(
		'attr' => array(
		'class' => 'total_order',
		'placeholder' => 'Total'
		),
		'required' => true,
		'disabled' => true,
		'label' => 'Total'
		));
		$builder->add('submit','submit', array(
		'attr' => array(
		'class' => 'btn btn-primary',
		),
		'label' => 'Update'
		));
		$builder->get('products')
		    ->addModelTransformer(new OrderProductTransformer($this->entityManager, $this->order))
		    ->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
		        $form = $event->getForm();
		        $data = $event->getData();
		        $result = array();
		        if ($data) {
		            foreach ($data as $key => $value) {
		                $result[] = array(
		                    'key' => $key,
		                    'value' => $value
		                );
		            }
		        }
		        $event->setData($result);
		    }, 1);
		}

    public function getName()
    {
        return 'orders';
    }
     public function configureOptions(OptionsResolver $resolver)
	{
	    $resolver->setDefaults(
	    	array(
	        'data_class' => 'Sibneuro\SiteBundle\Entity\Order',
	    ));
	}
}
