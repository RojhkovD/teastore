<?php

namespace Sibneuro\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      
	  $builder->add('email','email',array(
	  'attr' => array(
	  'id' => 'mail',
	  'placeholder' => 'Email',
	  ),
	  'required' => true
	  )
	  );
	  $builder->add('firstname','text',array(
	  'attr' => array(
	  'placeholder' => 'Firstname',
	  'id' => 'firstname',
	  ),
	  
	  'required' => true
	  )
	  );
	  $builder->add('lastname','text', array(
	  'attr' => array(
	  'placeholder' => 'Lastname',
	  'id' => 'lastname',
	  ),
	  
	  'required' => false
	  
	  )
	  
	  
	  );
	  $builder->add('password','password', array(
    'attr' => array(
        'placeholder' => 'Password',
		'id' => 'password',
    ),
	'required' => true
	)
);
	  
	  $builder->add('dob','date', array(
    'attr' => array(
		'id' => 'dob',
    ),
	)
);
	  
	  
	  
	  
	  
	  $builder->add('gender', 'choice', array(
    'choices' => array(
        'male' => 'Male',
        'female' => 'Female',
        
    ),
    'required'    => false,
    'empty_value' => 'Choose your gender',
    'empty_data'  => null,
	'attr' => array(
	'id' => 'gender',
)));
        
     
    }

    public function getName()
    {
        return 'users';
    }
     public function getDefaultOptions(array $options)
{
    return array(
        'data_class' => 'Project\UsersBundle\Entity\Users',
    );
}
}
