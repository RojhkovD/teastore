<?php

namespace Sibneuro\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PagesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder->add('c','hidden', array(
	  'attr' => array(
	  'id' => 'c_page',
	  ),
	  'mapped' => false));
	  $builder->add('pgname','text',array(
	  'attr' => array(
	  'id' => 'pgname_pg',
	  'placeholder' => 'Pagename',

	  ),
	  'required' => true,
	  'label' => false
	  )
	  );
	  $builder->add('createdBy','text',array(
	  'attr' => array(
	  'id' => 'createdBy_pg',
	  'placeholder' => 'CreatedBy',

	  ),
	  'required' => true,
	  'label' => false
	  )
	  );
	  $builder->add('pgmeta','textarea',array(
	  'attr' => array(
	  'id' => 'pgmeta_pg',
	  'placeholder' => 'Meta',

	  ),
	  'required' => false,
	  'label' => false
	  )
	  );
	  $builder->add('pgcontent', null, array(
	  'attr' => array(
	  'class' => 'ckeditor',
	  'placeholder' => 'Content...',
	  'id' => 'content_pg',
	  ),
	  
	  'required' => true,
	  'label' => false
	  ));
	  $builder->add('submit','submit', array(
	  'attr' => array(
	  'class' => 'btn btn-primary',
	  ),
	  'label' => 'Create'
	  ));
        
     
    }

    public function getName()
    {
        return 'pages';
    }
     public function getDefaultOptions(array $options)
{
    return array(
        'data_class' => 'Sibneuro\SitesBundle\Entity\Page',
    );
}
}
