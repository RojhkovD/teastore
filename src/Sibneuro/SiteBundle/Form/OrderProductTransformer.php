<?php
namespace Sibneuro\SiteBundle\Form;

use Sibneuro\SiteBundle\Entity\OrderProduct;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class OrderProductTransformer implements DataTransformerInterface
{
    private $entityManager;


    private $order;

    private $id;

    private $products;

    private $amount;

    public function __construct(EntityManager $entityManager, $order)
    {
        $this->entityManager = $entityManager;
        $this->order = $order;
        $this->id = $this->order->getId();
    }

    /**
     * 
     *
     * @param  OrderProduct|null $orderproducts
     * @return string
     */
    public function transform($orderproducts)
    {
        if(!empty($orderproducts)){
            $this->id = $orderproducts[0]['value']->getOrder()->getId();
            foreach($orderproducts as $key => $value){
                $this->amount[$key] = $value['value']->getAmount();
                $this->products[$key] = $value['value']->getProduct();
                $arr[] = array('name' => $key, 'value' => $value['value']->getProduct()->getProductname(), 'amount' => $value['value']->getAmount(), 'price' => $value['value']->getProduct()->getPrice());
            }   
        }else{
            return null;
        }

        return $arr;
    }


    public function replaceEqual($orderproducts){
        if(!empty($orderproducts)){
            $products = array();
            $result = array();

            //  replace equal orderproducts by single orderproduct with overall amount 
            foreach ($orderproducts as $orderproduct_key => $orderproduct_value) {
                if(!isset($products[$orderproduct_value->getProduct()->getProductname()])){
                    $products[$orderproduct_value->getProduct()->getProductname()] = $orderproduct_value;
                }else{
                    $products[$orderproduct_value->getProduct()->getProductname()]->setAmount($products[$orderproduct_value->getProduct()->getProductname()]->getAmount() + $orderproduct_value->getAmount());
                }
            }
            $i = 0;
            foreach($products as $key => $value){
                $result[$i] = $value;
                $i++;
            }
            return $result;
        }
    }

    /**
     * 
     *
     * @param  array $orderproducts_arr
     * @return OrderProduct|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($products_name_arr){
        // get order if exists
        $order = $this->order;

        $id_products = array();
        $orderproductOld = array();
        $orderproducts = array();

        $products = array();
        $products_amount = array();

        if (!$products_name_arr) {
            return;
        }

        //get all products from page
        foreach($products_name_arr as $key => $p){
            $products[$key] = $this->entityManager
                ->getRepository('SibneuroSiteBundle:Product')
                ->findOneByProductname($p['value']);
            $products_amount[$key] = $p['amount'];
        }


        //get id of products
        foreach($products as $key => $value){
            if(!empty($value)){
                $id_products[$key] = $value->getId();
            }
        }

        if($order){


            $orderproductsOld = $this->entityManager->getRepository('SibneuroSiteBundle:OrderProduct')
                ->findBy(
                    array('order' => $this->id));

            //delete old OrderProducts if exist
            foreach ($orderproductsOld as $op => $op_value) {
                $this->entityManager->remove($op_value);
                unset($orderproductsOld[$op]);
            }

            //set new OrderProducts
            $i = 0;
            foreach ($products as $key => $value) {
                $orderproducts[$i] = new OrderProduct();
                $orderproducts[$i]->setProduct($value);
                $orderproducts[$i]->setOrder($order);
                $orderproducts[$i]->setAmount($products_amount[$key]);
                $i++;
            }

            //group similar orderproducts
            $orderproducts = $this->replaceEqual($orderproducts);

            //persist
            foreach ($orderproducts as $key => $value) {
                $this->entityManager->persist($value);
            }

        }
        return $orderproducts;
    }
}

?>
