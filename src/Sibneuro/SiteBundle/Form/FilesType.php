<?php

namespace Sibneuro\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class FilesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder->add('c','hidden', array(
	  'attr' => array(
	  'id' => 'c_file',
	  ),
	  'mapped' => false));
	  $builder->add('filename','text',array(
	  'attr' => array(
	  'id' => 'filename_file',
	  'placeholder' => 'Filename',

	  ),
	  'required' => true,
	  'label' => false
	  )
	  );
	  $builder->add('file','file', array(
	  'attr' => array(
	  'placeholder' => 'Upload file',
	  'id' => 'file',
	  ),
	  
	  'required' => false,
	  'label' => 'Upload file'
	  ));
	  $builder->add('submit','submit', array(
	  'attr' => array(
	  'class' => 'btn btn-primary',
	  ),
	  'label' => 'Upload'
	  ));
        
     
    }

    public function getName()
    {
        return 'files';
    }
     public function getDefaultOptions(array $options)
{
    return array(
        'data_class' => 'Sibneuro\SiteBundle\Entity\File',
    );
}
}
