<?php

namespace Sibneuro\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder->add('c','hidden', array(
	  'attr' => array(
	  'id' => 'c_user',
	  ),
	  'mapped' => false));
	  $builder->add('username','text',array(
	  'attr' => array(
	  'id' => 'username_user',
	  'placeholder' => 'Username',

	  ),
	  'required' => true,
	  'label' => false
	  )
	  );
	  $builder->add('fullname','text',array(
	  'attr' => array(
	  'placeholder' => 'Fullname',
	  'id' => 'fullname_user',
	  ),
	  
	  'required' => false,
	  'label' => false
	  ));
	  $builder->add('email','email', array(
	  	'attr' => array(
	  		'id' =>  'email_user',
	  		'placeholder' => 'Email',
	  		),
	  	'required' => true,
	  	'label' => false
	  	)
	  );
	  $builder->add('password','password', array(
	  	'attr' => array(
	  		'id' =>  'password_user',
	  		'placeholder' => 'Password',
	  		),
	  	'required' => true,
	  	'label' => false
	  	)
	  );
	  $builder->add('address','text', array(
	  'attr' => array(
	  'placeholder' => 'Address',
	  'id' => 'address_user',
	  ),
	  
	  'required' => false,
	  'label' => "Address"
	  ));
	  $builder->add('phone','text', array(
	  'attr' => array(
	  'placeholder' => 'Phone number',
	  'id' => 'phone_user',
	  ),
	  
	  'required' => false,
	  'label' => "Phone number"
	  ));
	  $builder->add('role','choice', array(
	  'attr' => array(
	  'id' => 'acess_user'),
	  'choices' => array(
	  	'ROLE_ADMIN' => 'Admin',
	  	'ROLE_USER' => 'User',
	  ),
	  'required' => true,
	  'empty_value' => 'Access role',
	  'label' => false
	  ));
	  $builder->add('isactive','checkbox', array(
	  'label' => 'Active',
	  'required' => false,
	  ));
	  $builder->add('file','file', array(
	  'attr' => array(
	  'placeholder' => 'Upload photo',
	  'id' => 'photo_user',
	  ),
	  
	  'required' => false,
	  'label' => 'Upload photo'
	  ));
	  $builder->add('submit','submit', array(
	  'attr' => array(
	  'class' => 'btn btn-primary',
	  ),
	  'label' => 'Submit'
	  ));
        
     
    }

    public function getName()
    {
        return 'users';
    }
     public function getDefaultOptions(array $options)
{
    return array(
        'data_class' => 'Sibneuro\SiteBundle\Entity\User',
    );
}
}
