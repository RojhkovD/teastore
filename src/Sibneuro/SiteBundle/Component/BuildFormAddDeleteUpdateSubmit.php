<?php

namespace Sibneuro\SiteBundle\Component;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
  /* create checkbox array opposite data row*/
  class buildFormAddDeleteUpdateSubmit{
        private function buildFormAddDeleteUpdateSubmit(Request $request){
            $arr = array($check_user, $check_pages, $check_files);
            return  $this->createFormBuilder($arr)
                        ->setAction($this->generateUrl('/admin/search'))
                        ->setMethod('POST')
                        ->add("check_users[]", 'checkbox')
                        ->add("check_pages[]", 'checkbox')
                        ->add("check_files[]", 'checkbox')
                        ->getForm();
        }

}


?>