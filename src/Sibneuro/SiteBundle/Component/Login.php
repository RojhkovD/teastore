<?php


namespace Sibneuro\SiteBundle\Component;

class Login  {

		private $email;

		private $password;

		public function getEmail(){
			return $this->email;
		}
		public function getPassword(){
			return $this->password;
		}
		public function setEmail($email){
			$this->email = $email;
		}
		public function setPassword($password){
			$this->password = $password;
		}

}

