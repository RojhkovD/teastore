<?php

namespace Sibneuro\SiteBundle\Component;

	class Paginator {

		private $limit;

		public $num;

		public $min;

		public $max;

		public $count;

		public $entity;

		public $route;

		private $pages;

		public $prop = array();

		public function __construct(){
			$this->limit = 1;
		}
		public function setLimits($limit, $num){
			$this->limit = $limit;
			$this->num = $num;
			$this->max = $limit;
			$this->min = ($num - 1) * $limit;

		}
		public function setCount($count){
			$this->count = $count;
			$this->pages = ceil($count/$this->limit);
		}
		public function setProp($arr){
			$i = null;
			
			if(isset($arr)&&( !isset($arr['request_status'])&&!isset($arr['status']) )){
			if(count($arr)==1)
			foreach($arr as $a)
				foreach($a as $r => $val){
				$i++;
				$p_name = 'prop_'."$i";
			$this->prop[$r] = $val;
		}
			else
				foreach($arr as $a =>$val){
					$i++;
				$p_name = 'prop_'."$i";
			$this->prop[$a] = $val;
		}
	}
		}
		public function pagination(){
			$pages = $this->pages;
          	$num = $this->num;
          	$i = null;
          	$j = null;
          	$prev = null;
          	$next = null;

            if($num > 5 && $pages - $num > 3){
              $i = $num - 4;
              $j = $num + 2;
            }
            elseif($pages <= 8 ){
              $i = 1;
              $j = $pages;
            }
            elseif($pages - $num > 4){
              $i = 1;
              $j = 8;
            }
            else{
              $i = $pages - 7;
              $j = $pages;
            }
            
            if($num > 1)
            	$prev = $num - 1;
            	else
            	$prev = $this->num;
            if($num < $pages)
            	$next = $num + 1;
            	else
            	$next = $this->num;

            return array('start'=> $i, 'finish'=> $j, 'prev'=> $prev, 'next'=> $next);
        }


	}