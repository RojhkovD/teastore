<?php


namespace Sibneuro\SiteBundle\Component;

use Symfony\Component\Validator\Constraints as Assert;

class SearchProperties  {


		/**
		* @Assert\NotBlank(message="Nothing to search")
		* @Assert\Length(min = 2, max = 50, minMessage = "Your query must be at least {{ limit }} characters long",
		* maxMessage = "Your query cannot be longer than {{ limit }} characters long")
		*/
		public $search;
		/**
		* @Assert\NotBlank(message="Nothing to search")
		* 
		*/
		public $category;

		/**
		* @Assert\DateTime()
		*/
		public $dateFrom;

		public function __construct(){
			$this->dateFrom = new \DateTime("1970-01-01 00:00:00");
			$this->category = "all";
		}
		
		
		public $dateString;
}

