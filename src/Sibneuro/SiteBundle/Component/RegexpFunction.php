<?php

namespace Sibneuro\SiteBundle\Component;

use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class RegexpFunction extends FunctionNode
{
    public $regexpExpression = null;
    public $valueExpression = null;

public function getSql(SqlWalker $sqlWalker)
{
    $returnString = '(' . $this->valueExpression->dispatch($sqlWalker) . '     REGEXP ' .
    $sqlWalker->walkStringPrimary($this->regexpExpression) . ')';

    return $returnString;
}

public function parse(Parser $parser)
{
    $parser->match(Lexer::T_IDENTIFIER);
    $parser->match(Lexer::T_OPEN_PARENTHESIS);
    $this->regexpExpression = $parser->StringPrimary();
    $parser->match(Lexer::T_COMMA);
    $this->valueExpression = $parser->StringExpression();
    $parser->match(Lexer::T_CLOSE_PARENTHESIS);
}
}

?>