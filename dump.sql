-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: teastore
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acme_users`
--

DROP TABLE IF EXISTS `acme_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acme_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_55884A7F85E0677` (`username`),
  UNIQUE KEY `UNIQ_55884A7E7927C74` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acme_users`
--

LOCK TABLES `acme_users` WRITE;
/*!40000 ALTER TABLE `acme_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `acme_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discounts`
--

DROP TABLE IF EXISTS `discounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bound` int(11) NOT NULL,
  `value` double NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discounts`
--

LOCK TABLES `discounts` WRITE;
/*!40000 ALTER TABLE `discounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `discounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `path_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_63540593C0BE965` (`filename`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (1,'test','2017-04-20 19:38:14','f0346f49e70d9ad1d1edf1c5fd63f08062eb97e0.jpg');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2530ADE68D9F6D38` (`order_id`),
  KEY `IDX_2530ADE64584665A` (`product_id`),
  CONSTRAINT `FK_2530ADE64584665A` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_2530ADE68D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_product`
--

LOCK TABLES `order_product` WRITE;
/*!40000 ALTER TABLE `order_product` DISABLE KEYS */;
INSERT INTO `order_product` VALUES (103,13,6,110),(104,13,7,78),(133,15,6,23),(134,15,7,1),(135,12,6,1),(136,12,7,23),(149,16,6,2),(150,16,7,1),(161,17,7,3),(162,17,6,4),(166,18,6,10),(167,18,7,14),(168,14,6,23),(169,14,7,3),(171,19,6,1),(172,19,7,3),(173,20,7,1),(174,21,7,1),(175,21,6,1),(176,22,6,1),(177,23,7,1),(178,23,8,1),(179,23,6,1),(180,23,9,1),(181,23,15,1);
/*!40000 ALTER TABLE `order_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `expires` datetime DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `total` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E52FFDEE9395C3F3` (`customer_id`),
  KEY `IDX_E52FFDEE4C7C611F` (`discount_id`),
  CONSTRAINT `FK_E52FFDEE4C7C611F` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`),
  CONSTRAINT `FK_E52FFDEE9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (12,4,NULL,'2017-04-13 14:40:00',NULL,'egerferfwef',1,800),(13,5,NULL,'2017-04-14 18:13:00',NULL,'wefwef',1,1200),(14,6,NULL,'2017-04-16 16:21:00',NULL,'erwfwergtheyrte ervrev',1,12700),(15,7,NULL,'2017-04-16 16:22:00',NULL,'wefwef',1,23422),(16,8,NULL,'2017-04-16 16:45:00',NULL,'ewrgwerfewrfrewf erwfwrefwe',1,2000),(17,9,NULL,'2017-04-16 18:13:00',NULL,'ergrewefwef',1,3200),(18,10,NULL,'2017-04-19 05:42:00',NULL,'ergerg',1,10600),(19,11,NULL,'2017-10-24 16:18:00',NULL,'ewfwefwe',1,1700),(20,12,NULL,'2017-10-24 16:19:44',NULL,'',1,0),(21,13,NULL,'2017-10-24 16:23:48',NULL,'ergreg',1,900),(22,14,NULL,'2017-10-24 18:54:43',NULL,'wefwefwed',1,500),(23,15,NULL,'2017-10-30 22:07:50',NULL,'wefwefwef',1,4600);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pgname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pgcontent` longtext COLLATE utf8_unicode_ci NOT NULL,
  `pgmeta` longtext COLLATE utf8_unicode_ci NOT NULL,
  `createdBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'main','<h1>Teastore main page</h1>\r\n\r\n<div>\r\n<p style=\"margin: 0.5em 0px; line-height: inherit; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;\"><b>Чай</b>&nbsp;(кит.&nbsp;<span lang=\"zh-cn\" xml:lang=\"zh-cn\">茶</span>&nbsp;&mdash; &laquo;ча́&raquo; на&nbsp;пекинском&nbsp;и&nbsp;гуандунском&nbsp;диалекте, &laquo;те̂&raquo; на&nbsp;амойском&nbsp;и &laquo;тцай-е&raquo; на&nbsp;тайваньском, &laquo;чай&raquo;&nbsp;&mdash; на хинди)&nbsp;&mdash;&nbsp;напиток, получаемый&nbsp;варкой,&nbsp;завариванием&nbsp;и/или&nbsp;настаиванием&nbsp;листа&nbsp;чайного куста, который предварительно подготавливается специальным образом.</p>\r\n\r\n<p style=\"margin: 0.5em 0px; line-height: inherit; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;\"><b>Чаем</b>&nbsp;также называется сам лист чайного куста, обработанный и подготовленный для приготовления напитка. Подготовка эта включает предварительную сушку (вяление), скручивание, более или менее длительное ферментативное окисление, окончательную сушку. Прочие операции вводятся в процесс только для производства отдельных видов и сортов чая.</p>\r\n\r\n<p style=\"margin: 0.5em 0px; line-height: inherit; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;\">Иногда слово &laquo;чай&raquo; используют и в качестве названия&nbsp;чайного куста&nbsp;&mdash;&nbsp;вида&nbsp;растений рода&nbsp;Камелия&nbsp;семейства&nbsp;Чайные; в ботанической научной литературе для этого вида обычно используется название камелия китайская (<i>Camellia sinensis</i>).</p>\r\n\r\n<p style=\"margin: 0.5em 0px; line-height: inherit; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;\"><b>Чаем</b>&nbsp;в широком смысле может именоваться любой напиток, приготовленный путём заваривания предварительно подготовленного растительного материала. В названиях таких напитков к слову &laquo;чай&raquo;, как правило, добавляется пояснение, характеризующее используемое сырьё (&laquo;травяной чай&raquo;, &laquo;ягодный чай&raquo;, &laquo;фруктовый чай&raquo; и так далее).</p>\r\n\r\n<p style=\"margin: 0.5em 0px; line-height: inherit; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;\">&nbsp;</p>\r\n\r\n<p style=\"margin: 0.5em 0px; line-height: inherit; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;\"><img alt=\"Tea-image\" src=\"/uploads/pages/direct-38-6.jpg\" style=\"float: left; width: 360px; height: 239px;\" /></p>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Выращивание и сбор&nbsp;</h2>\r\n\r\n<div style=\"margin: 0.5em 0px; line-height: inherit; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;\">\r\n<p>Сырьём для изготовления чая являются листья&nbsp;чайного куста, который выращивают в массовом количестве на специальных плантациях. Для произрастания этого растения необходим тёплый климат с достаточным количеством влаги, не застаивающейся у корней. Большинство чайных плантаций располагается на горных склонах в районах с&nbsp;тропическим&nbsp;или&nbsp;субтропическим&nbsp;климатом. В&nbsp;Китае,&nbsp;Индии&nbsp;и&nbsp;Африке, где производится наибольшая доля чая, сбор проводится до четырёх раз в год. Наиболее ценятся чаи первых двух урожаев. Северная граница территории, на которой выращивание чая экономически оправдано, проходит приблизительно на широте&nbsp;Грузии&nbsp;и&nbsp;Краснодарского края&nbsp;России. В более высоких широтах чайный куст ещё может произрастать, но культивировать его в целях заготовки чая убыточно.</p>\r\n\r\n<p>Листья чая собираются и сортируются вручную: для чаёв наиболее высокой сортной категории используются нераспустившиеся или распустившиеся почки и самые молодые листья, лишь первая-вторая флешь (первая-вторая группа листьев на побеге, считая от конца); более &laquo;грубые&raquo; чаи делают из зрелых листьев. Труд сборщиков достаточно тяжёлый и монотонный: соотношение массы готового чёрного чая и сырого листа&nbsp;&mdash; около &frac14;, то есть на изготовление килограмма чая требуется собрать четыре килограмма листа. Норма выработки для сборщиков составляет 30&mdash;35&nbsp;кг листа в день, при том, что необходимо соблюдать стандарты качества и брать с кустов только нужные листья. Сырьё для высокосортных чаёв часто растёт на небольших плантациях (площадью около 0,5&nbsp;га), располагающихся разрозненно на горных склонах, так что к сборке листа добавляется необходимость перехода с одной плантации на другую. Современные промышленные плантации делают, как правило, достаточно крупными, чтобы обеспечить непрерывность сборки и поднять производительность, собранный на этих плантациях лист идёт на чай массового производства.</p>\r\n\r\n<p>Необходимость ручной сборки ограничивает возможности культивирования чая: оно имеет смысл только в регионах с достаточно высокой продуктивностью и достаточно низкой стоимостью ручного труда сборщиков. Неоднократно делались попытки механизировать сборку и сортировку чайного листа, в частности, в&nbsp;СССР&nbsp;ещё в&nbsp;1958 году&nbsp;был создан механизированный чаеуборочный агрегат, однако технология механизированной сборки до сих пор не доработана: собранный комбайнами лист имеет слишком низкое качество, главным образом за счёт большого количества посторонних включений (побеги, засохшие листья, посторонний мусор и так далее), так что используется он либо для производства самого низкосортного чая, либо в фармацевтической промышленности, для последующей переработки с целью выделения&nbsp;кофеина&nbsp;и других содержащихся в чае веществ</p>\r\n</div>','wedqwed','Dmitry','2017-04-12 02:02:56','2017-10-29 23:13:40'),(2,'contacts','<p>Наш адрес: Челюскинцев 44</p>\r\n\r\n<p>Телефон: &nbsp; +7(383)-344-74-76</p>\r\n\r\n<p>&nbsp;</p>','Contacts','Dmitry','2017-04-20 15:24:42','2017-04-20 15:25:29');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productComments`
--

DROP TABLE IF EXISTS `productComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productComments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `customer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3790EB14584665A` (`product_id`),
  CONSTRAINT `FK_3790EB14584665A` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productComments`
--

LOCK TABLES `productComments` WRITE;
/*!40000 ALTER TABLE `productComments` DISABLE KEYS */;
/*!40000 ALTER TABLE `productComments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `path_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `category` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `instock` tinyint(1) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sold` int(11) NOT NULL,
  `viewed` int(11) NOT NULL,
  `gold` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B3BA5A5AF36733E3` (`productname`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (6,'Ветер перемен','2017-04-12 17:38:12','726b3ce4ee0cc37ccc88ac2e068834bb33a66122.jpg','Травяной чай',500,1,'Превратить привычное чаепитие в полезное – просто и легко! Откройте для себя чайный напиток «Витаминный коктейль». В его составе травы Алтая (листья смородины чёрной, мяты,  плоды рябины черноплодной, шиповника, рябины обыкновенной, яблоки нарезанные, плоды, листья облепихи, малины, мелисса, цветки календулы), хранящие в себе кладезь полезных витаминов и веществ. Все компоненты чая выросли  в диких условиях в экологически чистых районах края и бережно собраны вручную для Вас. Здоровье, красота и отличное самочувствие – в каждой чашке «Витаминного коктейля»!',0,0,0),(7,'Лето','2017-04-12 17:42:33','1a0872b96bc72b617485331f3ed244466e10831c.jpg','Зеленый чай',400,1,'Чай зеленый, индийский, элитных сортов.\r\nДля него характерен пряный, слегка цветочный аромат и легкое медовое послевкусие.',0,0,0),(8,'Краски Индии','2017-10-29 11:53:45','b27cf282be3438916d3d155f19b5e665d8493e97.jpg','Зеленый чай',1900,1,'Аюрведический чай является уникальной возможностью добиться гармонии со своим организмом. Аюрведа считает, что ключ к  долголетию — это сохранение и восстановление баланса организма. Аюрведические чаи Organic India, приготовленные с использованием  органически чистых индийских трав, оказывают освежающее, бодрящее или успокаивающее (в зависимости от добавки) действие.',0,0,0),(9,'Королевский каркаде','2017-10-29 11:58:30','337b78552be119a70d36c758b5224d965124ce84.jpg','Зеленый чай',1000,1,'Фруктовый чай. Сладко-кислый напиток, насыщенного красного цвета из гибискуса.',0,0,0),(10,'Наглый фрукт','2017-10-29 12:05:47','95664f66b313ebef98bf12d1babffcf2df40b842.jpg','Зеленый чай',1200,1,'Легкий, с превосходными вкусовыми качествами чай. Состоит из смеси ломтиков фруктов: ананаса, яблока, вишни, папайи, винограда, малины, а также плодов шиповника и цветов гибискуса.',0,0,0),(11,'Ромашковый чай','2017-10-29 12:09:53','cb5f70d57edc9f1084a8bec48b0032fff27b346b.jpg','Травяной чай',500,1,'Скромные цветы, часто растущие не только на лугах, но и на обочинах дорог в пыли и забвении, на самом деле являются источником поистине бесценного сырья. Ромашковый отвар способен избавлять от многих болезней и защищать от «нашествия» микробов.',0,0,0),(12,'Китайский чай с жасмином','2017-10-29 12:39:55','dfb339ffae13d25178957d4260d58a889a70a080.jpg','Зеленый чай',1900,1,'Чай с цветками жасмина. Чай с жасмином пользуется большой популярностью в Китае с древних времен. Жасмин чаще всего добавляют в зеленый и белый чай, иногда в черный. Цветки жасмина являются природным натуральным ароматизатором чая.',0,0,0),(13,'Бриз','2017-10-29 12:43:32','d85cda49b035c6ca69c97a59be8606acc6e76f0b.jpg','Зеленый чай',1200,1,'Чай с мятой. Ароматный, нежный и освежающий напиток, дарящий прохладу. Он освежает, придает силы, улучшает пищеварение. В то же время чай с мятой снимает нервное напряжение, помогает расслабиться и забыть о проблемах.',0,0,0),(14,'Приключение','2017-10-29 12:48:39','45da75a5d9d87c071f6e55285b0321a7be0346ca.jpg','Черный чай',500,1,'Гибискус, ягоды барбариса, черный чай.',0,0,0),(15,'Мечта','2017-10-29 12:52:45','f4ec94b46c7b288209679702b55e8fdf9a8df68c.jpg','Черный чай',800,1,'Черный чай с жасмином',0,0,0);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8_unicode_ci,
  `phone` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `avatar_path` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'User1','$2y$13$0cRuHA7iO5h47ylVIZ8ZEuTroXrbTfVmmp/kw9iHNW1.pPuBzRZNO','anymail@example.com','fullname','City, Street 1',7,'ROLE_USER','2017-04-11 18:30:20',1,'a66ed015e80680fe1541ab535f26b8d2c23e74d9.jpg'),(2,'Dmitry',NULL,'wannabetrix@mail.ru',NULL,NULL,7,NULL,'2017-04-13 03:44:42',1,NULL),(3,'Dmitry',NULL,'wqafew@wef.ewe',NULL,NULL,23423423,NULL,'2017-04-13 13:20:04',1,NULL),(4,'wefqwedw',NULL,'ewefwe@wef.wewe',NULL,NULL,23423432,NULL,'2017-04-13 14:40:49',1,NULL),(5,'wefwed',NULL,'weewf@fwef.wef',NULL,NULL,234234,NULL,'2017-04-14 18:13:39',1,NULL),(6,'wefwef',NULL,'wefwef@wefw.wef',NULL,NULL,234324,NULL,'2017-04-16 16:21:52',1,NULL),(7,'wedwe23fwefwef',NULL,'wefwef@ewf.wef',NULL,NULL,234234,NULL,'2017-04-16 16:22:48',1,NULL),(8,'wefwef','123123123','ewfwef@ewfwf.wef',NULL,NULL,234234,'ROLE_USER','2017-04-16 16:45:21',1,'6c5d6fd9f824aad9efdc4dd84f947532851d72d0.png'),(9,'Василий','23423423423','rref@ewfwe.wef','Василий Иванов','City Street 2332e32',23423423,'ROLE_USER','2017-04-16 18:13:33',1,NULL),(10,'rgefwef',NULL,'efew@ewfewf.ewf',NULL,NULL,34534543,NULL,'2017-04-19 05:42:52',1,NULL),(11,'ewfewf',NULL,'wed@wefew.ewf',NULL,NULL,34534534,NULL,'2017-10-24 16:18:16',1,NULL),(12,'fwefwef',NULL,'wefreg@efewf.wef',NULL,NULL,324234234,NULL,'2017-10-24 16:19:44',1,NULL),(13,'regreg',NULL,'wefewf2@wef.wef',NULL,NULL,234234234,NULL,'2017-10-24 16:23:48',1,NULL),(14,'reger',NULL,'fhrthrth@ewf.wefwef',NULL,NULL,33234234,NULL,'2017-10-24 18:54:43',1,NULL),(15,'wefqwe',NULL,'sdf@ewfwe.ewf',NULL,NULL,23423432,NULL,'2017-10-30 22:07:50',1,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-31  1:40:55
